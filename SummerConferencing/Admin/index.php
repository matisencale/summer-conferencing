<?php
include("./admin_api.php");

printDocHeading();
print" <body>\n";
printNavBar();
print"     <!-- JQuery -->";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.10.5/jquery.dataTables.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>";
print"  <script type='text/javascript' charset='utf8' src='../TableTools-2.2.3/js/dataTables.tableTools.js'></script>\n";

print" </body>\n";
printFooter();
print" </html>\n";						

function printNavBar() {

print" <div class='bs-example'>\n";
print"    <nav role='navigation' class='navbar navbar-custom navbar-fixed-top'>\n";
print"        <!-- Brand and toggle get grouped for better mobile display -->\n";
print"        <div class='navbar-header'>\n";
print"            <button type='button' data-target='#navbarCollapse' data-toggle='collapse' class='navbar-toggle'>\n";
print"                <span class='sr-only'>Toggle navigation</span>\n";
print"                <span class='icon-bar'></span>\n";
print"                <span class='icon-bar'></span>\n";
print"                <span class='icon-bar'></span>\n";
print"            </button>\n";
print"            <a href='http://www.cs.stedwards.edu/~mharper5/SummerConferencing/index2.php' class='navbar-brand'>Home</a>\n";
print"        </div>\n";
print"        <!-- Collection of nav links, forms, and other content for toggling -->\n";
print"        <div id='navbarCollapse' class='collapse navbar-collapse'>\n";
print"            <ul class='nav navbar-nav'>\n";
print"                <li class='active'><a href='#'>Conferences</a></li>\n";
print"                <li><a href='#'>Residence Halls</a></li>\n";
print"            </ul>\n";
print"            <ul class='nav navbar-nav navbar-right'>\n";
print"                <li><a href='#'>Login</a></li>\n";
print"            </ul>\n";
print"            <form role='search' class='navbar-form navbar-right'>\n";
print"                <div class='form-group'>\n";
print"                    <input type='text' placeholder='Find Guest' class='form-control'>\n";
print"                </div>\n";
print"            </form>\n";
print"        </div>\n";
print"        </div>\n";
print"    </nav>\n";
print" </div>\n";
}


?>