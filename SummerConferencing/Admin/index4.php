<?php
include("./admin_api2.php");

printDocHeading();
print" <body>\n";
printNavBar();
print"     <!-- JQuery -->";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.10.5/jquery.dataTables.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>";
print"  <script type='text/javascript' charset='utf8' src='../TableTools-2.2.3/js/dataTables.tableTools.js'></script>\n";

print" </body>\n";
printFooter();
print" </html>\n";						

function printDocHeading() {
print" <!DOCTYPE html>\n";
print" <html lang='en'\n>";
print" <head>\n";
print" <meta charset='UTF-8'>\n";
print" <title>SEU Summer Conferencing Admin</title>\n";
print" <link rel='stylesheet' href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>\n";

print" <link rel='stylesheet' type='text/css' href='../TableTools-2.2.3/css/dataTables.tableTools.css'>";

print" <link rel='stylesheet' href='../bootstrap-3.3.2/dist/css/bootstrap-theme.min.css'>\n";
print" <link rel='stylesheet' href='../bootstrap-3.3.2/dist/css/bootstrap.custom.css'>\n";
print" <link rel='stylesheet' type='text/css' href='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css'>";

print" <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>\n";
print" <script src='../bootstrap-3.3.2/dist/js/bootstrap.min.js'></script>\n";

print" </style>\n";
print" </head>\n";
}

function printFooter() {
print" <footer class='footer'>";
print" <div class=' navbar-fixed-bottom'>";
print" <div class='container'>";
print" <div class='col-4 col-offset-4' style='text-align:center;'>";
print"   <p style='vertical-align:middle;'><a href='#'class='backToTop'>Back To Top</a></p>";
print" </div>";
print" </div>";
print"    </footer>";

print"<script type='text/javascript'>";
print"jQuery('.backToTop').click(function(){";
print"	jQuery('html, body').animate({scrollTop:0}, 'slow');";
print"});";
print"</script>";
//print"<script>";
//print"$('body').css('overflow','hidden');";
//print"$('body').css('overflow','auto');";
//print</script>":
}

function printNavBar() {

print" <div class='bs-example'>\n";
print"    <nav role='navigation' class='navbar navbar-custom navbar-fixed-top'>\n";
print"        <!-- Brand and toggle get grouped for better mobile display -->\n";
print"        <div class='navbar-header'>\n";
print"            <button type='button' data-target='#navbarCollapse' data-toggle='collapse' class='navbar-toggle'>\n";
print"                <span class='sr-only'>Toggle navigation</span>\n";
print"                <span class='icon-bar'></span>\n";
print"                <span class='icon-bar'></span>\n";
print"                <span class='icon-bar'></span>\n";
print"            </button>\n";
print"            <a href='http://www.cs.stedwards.edu/~mharper5/SummerConferencing/index2.php' class='navbar-brand'>Home</a>\n";
print"        </div>\n";
print"        <!-- Collection of nav links, forms, and other content for toggling -->\n";
print"        <div id='navbarCollapse' class='collapse navbar-collapse'>\n";
print"            <ul class='nav navbar-nav'>\n";
print"                <li class='active'><a href='#'>Conferences</a></li>\n";
print"                <li><a href='#'>Residence Halls</a></li>\n";
print"            </ul>\n";
print"            <ul class='nav navbar-nav navbar-right'>\n";
print"            </ul>\n";
print"            <ul class='nav navbar-nav navbar-right'>\n";
print"                <li><a href='#'>Login</a></li>\n";
print"            </ul>\n";
print"            <form role='search' class='navbar-form navbar-right'>\n";
print"                <div class='form-group'>\n";
print"                    <input type='text' placeholder='Find Guest' class='form-control'>\n";
print"                </div>\n";
print"            </form>\n";
print"        </div>\n";
print"        </div>\n";
print"    </nav>\n";
print" </div>\n";

if(empty ($_POST))
{
viewConferenceGroupTable();
getConferenceGroupDataTable();
viewConferenceTables();
print"<br></br>";
}
else if($_POST['conferenceGroupAddFormSubmit'])
{
addConferenceGroup($_POST['addConferenceGroupName']);
viewConferenceGroupTable();
getConferenceGroupDataTable();
viewConferenceTables();
print"<br></br>";
}
else if($_POST['conferenceAddFormSubmit'])
{
addConference($_POST['conferenceGroupID'],$_POST['addConferenceName']);
viewConferenceGroupTable();
getConferenceGroupDataTable();
viewConferenceTables();
print"<br></br>";
}

else if($_POST['selectConferenceSubmit'])
{
print "<br></br>";
print "<br></br>";
viewGuestListTable($_POST['conferenceselectioninput']);
getGuestListDataTable();
showAddGuestForm($_POST['conferenceselectioninput']);
showAssignRoomForm($_POST['conferenceselectioninput']);

showRosterTable($_POST['conferenceselectioninput']);
getDataTableRoster1();
addGuestToRosterForm();
showCheckInForm();
showCheckOutForm();
$conferenceID=20;
}
else if(isset($conferenceID))
{
print "<br></br>";
print "<br></br>";
viewGuestListTable($_POST['conferenceselectioninput']);
getGuestListDataTable();
showAddGuestForm($_POST['conferenceselectioninput']);
showAssignRoomForm($_POST['conferenceselectioninput']);

showRosterTable($_POST['conferenceselectioninput']);
getDataTableRoster1();
addGuestToRosterForm();
showCheckInForm();
showCheckOutForm();
}

else if ($_POST['selectRosterGuestCheckInSubmit'])
{
guestCheckIn($_POST['rosterguestid']);
print "<br></br>";
showRosterTable($_POST['conferenceselectioninput']);
getDataTableRoster1();
addGuestToRosterForm();
showCheckInForm();
showCheckOutForm();
}
else if ($_POST['selectRosterGuestCheckOutSubmit'])
{
guestCheckOut($_POST['rosterguestid']);
print "<br></br>";
showRosterTable($_POST['conferenceselectioninput']);
getDataTableRoster1();
addGuestToRosterForm();
showCheckInForm();
showCheckOutForm();
}

//print" </div>";
}


?>