<?php
include("./admin_api2.php");

printDocHeading();
print" <body>\n";
print"     <!-- JQuery -->";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.10.5/jquery.dataTables.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>";
print"  <script type='text/javascript' charset='utf8' src='../TableTools-2.2.3/js/dataTables.tableTools.js'></script>\n";
printNavBar();
print" </body>\n";
printFooter();
print" </html>\n";						

function printDocHeading() {
print" <!DOCTYPE html>\n";
print" <html lang='en'\n>";
print" <head>\n";
print" <meta charset='UTF-8'>\n";
print" <title>SEU Summer Conferencing Admin</title>\n";
print" <link rel='stylesheet' href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>\n";



print" <link rel='stylesheet' href='../bootstrap-3.3.2/dist/css/bootstrap-theme.min.css'>\n";
print" <link rel='stylesheet' href='../bootstrap-3.3.2/dist/css/bootstrap.custom.css'>\n";
print" <link rel='stylesheet' type='text/css' href='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css'>";

print" <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>\n";
print" <script src='../bootstrap-3.3.2/dist/js/bootstrap.min.js'></script>\n";

print" </style>\n";
print" </head>\n";
}

function printFooter() {
print" <footer class='footer'>";
print" <div class=' navbar-fixed-bottom'>";
print" <div class='container'>";
print" <div class='col-4 col-offset-4' style='text-align:center;'>";
print"   <p style='vertical-align:middle;'><a href='#'class='backToTop'>Back To Top</a></p>";
print" </div>";
print" </div>";
print"    </footer>";

print"<script type='text/javascript'>";
print"jQuery('.backToTop').click(function(){";
print"	jQuery('html, body').animate({scrollTop:0}, 'slow');";
print"});";
print"</script>";
//print"<script>";
//print"$('body').css('overflow','hidden');";
//print"$('body').css('overflow','auto');";
//print</script>":
}

function printNavBar() {

if(empty ($_POST))
{
viewConferenceGroupTable();
getConferenceGroupDataTable();
viewConferenceTables();
print"<br></br>";
}
else if($_POST['conferenceGroupAddFormSubmit'])
{
viewConferenceTables();
print"<br></br>";
}
else if($_POST['selectConferenceSubmit'])
{
print "<br></br>";
print "<br></br>";
viewGuestListTable($_POST['conferenceselectioninput']);
getGuestListDataTable();
showAddGuestForm($_POST['conferenceselectioninput']);
showAssignRoomForm($_POST['conferenceselectioninput']);

showRosterTable($_POST['conferenceselectioninput']);
getDataTableRoster1();
addGuestToRosterForm();
showCheckInForm();
showCheckOutForm();
}
else if ($_POST['selectRosterGuestCheckInSubmit'])
{
guestCheckIn($_POST['rosterguestid']);
print "<br></br>";
showRosterTable($_POST['conferenceselectioninput']);
getDataTableRoster1();
showCheckInForm();
showCheckOutForm();
}
else if ($_POST['selectRosterGuestCheckOutSubmit'])
{
guestCheckOut($_POST['rosterguestid']);
print "<br></br>";
showRosterTable($_POST['conferenceselectioninput']);
getDataTableRoster1();
showCheckInForm();
showCheckOutForm();
}

//print" </div>";
}


?>