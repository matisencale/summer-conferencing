<?php
include("../../adodb5/adodb.inc.php");

//Displays all Conferences seperated by conference group
function viewConferenceTables() {

//internal testing print statement
   //print "viewConferenceGroupTable()";

   //create db object
    $db = NewADOConnection('mysql');

    //query string
    $conference_group_query = "SELECT * FROM cosc4359_conference_group";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");


    //query results
    $conference_group_result = $db->Execute($conference_group_query);

    //db disconnect
    $db->Close();

    if ($conference_group_result === false) die("failed");

     while (!$conference_group_result->EOF) {
        for ($i=0, $max=$conference_group_result->FieldCount(); $i < $max; $i+=4)
				  		          print viewConferenceTable($conference_group_result->fields[$i+0]).
											 showConferenceSelectForm($conference_group_result->fields[$i+0]).
//										         showConferenceAddForm();
											 print "</div>";
															  
															      $conference_group_result->MoveNext();
        }
	
    print" <style type='text/css'>";
    print"    .table-window-fit{";
    print"    margin: 20px;";
    print"    }";
    print" </style>";
}

function viewConferenceGroupTable() {
   
   //internal testing print statement   
   //print "viewConferenceGroupTable()";

   //create db object
    $db = NewADOConnection('mysql');

    //query string
    $conference_group_query = "SELECT * FROM cosc4359_conference_group";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $conference_group_result = $db->Execute($conference_group_query);

    //db disconnect
    $db->Close();

    if ($conference_group_result === false) die("failed");

    

    $conferenceGroupTableString = "<div class='table-window-fit'>\n".
    				  	  "<br></br>".
							  "<h2>Conference Group List</h2>".
                          "<table id='conferencegrouptable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
//                          "         <th>Conference GroupID</th>\n".
                          "         <th>Conference Group - Select a Conference Group</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
//                          "         <th>Conference Group ID</th>\n".
                          "         <th>Conference Group</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with information from database-->\n";

    while (!$conference_group_result->EOF) {
        for ($i=0, $max=$conference_group_result->FieldCount(); $i < $max; $i+=4)
            $fieldResultConferenceGroup.=  "<tr>\n".
//                                     "<td>".$conference_group_result->fields[$i+0]."</td>\n".
                                     "<td  id='".$conference_group_result->fields[$i+0]."'>".$conference_group_result->fields[$i+1]."</td>\n".
                                  "</tr>\n";
            $conference_group_result->MoveNext();
        }
    $conferenceGroupTableString.= $fieldResultConferenceGroup;
    $conferenceGroupTableString.="       </tbody>\n".
                        "   </table>\n".
				"</div>";
    echo $conferenceGroupTableString;

    print"<div class='table-window-fit'>";
     showConferenceGroupAddForm();
    print"</div>";

    print" <style type='text/css'>";
    print"    .table-window-fit{";
    print"    margin: 20px;";
    print"    }";
    print" </style>";
}



//Prints javascript for DataTable Object Initialization
function getConferenceGroupDataTable() {
  
  //internal testing print statement
  //print "getConferenceGroupDataTable()";

print "<script>\n";
print "$(document).ready(function() {\n";
print "    var table1 = $('#conferencegrouptable').DataTable();\n";
print "    var tt = new $.fn.dataTable.TableTools( table1 );\n";
print "    $( tt.fnContainer() ).insertBefore('div.dataTables_wrapper');\n";

print "    $('#conferencegrouptable tbody').on( 'mouseenter', 'tr', function () {\n";
print "        $(this).toggleClass('active');\n";
print "    } );\n";
print "    $('#conferencegrouptable tbody').on( 'mouseleave', 'tr', function () {\n";
print "        $(this).toggleClass('active');\n";
print "    } );\n";

print "    $('#conferencegrouptable tr').on( 'click','td', function () {";
print "    ar2 = $(this).attr('id');";
print "        console.log(ar2);";
print "    tablestring1 = '#conference-table-';";
print "    tablestring2 = ar2;";
print "	      fulltablestring = tablestring1.concat(tablestring2);";
print "	         console.log(fulltablestring);";
//print "    var table-heading-class=";
print "    document.location.href=fulltablestring;";

//print "        console.log(ar2);";

print "    } );";

print "} );\n";
print "</script>\n";
}



function showConferenceGroupSelectForm() {

  print "<div> <form method='post' action='$self' >\n";
  print "<input type='hidden' id='conference-group-selection-input' name='conference-group-selection-input' value='' />\n";
  print "<input type='submit' name='selectConferenceGroupSubmit' value='View selected conference group' />\n";
  print "</form>\n</div>\n";
  print "<br></br>";

}



function showConferenceGroupAddForm() {

 $self = $_SERVER['PHP_SELF'];

  print "<div> <form method='post' action='$self' >\n";
  print "<label for='conference-group-option'>Name of Conference Group: </label>\n";
  print "<input type='text' name='addConferenceGroupName' value=''>\n";
  print "<input type='submit' name='conferenceGroupAddFormSubmit' ".
        " value='Add Conference Group' />\n";
  print "</form>\n</div>\n";

}


function addConferenceGroup($conferenceGroupName) {

  //internal testing print statement
  //print "addConferenceGroup(".$conferenceGroupName.")";

  //print $conferenceGroupName;
  $self = $_SERVER['PHP_SELF'];
  $db = NewADOConnection('mysql');
  $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
  $sql = "INSERT INTO cosc4359_conference_group (conference_group_name) VALUES ('". $conferenceGroupName."')";
  $db->Execute($sql);

 if ($result === false) die("failed");
}



function guestCheckIn($guest_id) {

$db = NewADOConnection('mysql');
 $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
 $query="UPDATE `cosc4359_roster` SET check_in = current_timestamp where guest_id=".$guest_id;
 //echo $query;
 $result = $db->Execute($query);

 if ($result === false) die("failed");
}



function guestCheckOut($guest_id) {

$db = NewADOConnection('mysql');
 $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
 $query="UPDATE `cosc4359_roster` SET check_out = current_timestamp where guest_id=".$guest_id;
 //echo $query;
 $result = $db->Execute($query);

 if ($result === false) die("failed");

}

function showAddGuestForm($conferenceGroupID) {

print" <div>";
print" <form action='add_guest.php' method='post'>";
print"  First Name: <input type='text' name='fname'>";
print"  Last Name: <input type='text' name='lname'>";
print"  Gender: <input type='text' name='gender'>";
print"  Conference: <input type='text' name='conference_id' >";
//print"  Residence Hall: <input type='text' name='residence_hall_id'>";
//print"  Room: <input type='text' name='room_id'>";
print"  <input type='submit' value='Add Guest'>";
print"</form></div>";


}

function showAssignRoomForm($conferenceGroupID) {

print" <div>";
print" <form action='addRoomAssign.php' method='post'>";
print"  Guest ID: <input type='text' name='guestID'>";
print"  Room ID: <input type='text' name='roomID'>";
print"  <input type='submit' value='Assign Room to Guest'>";
print"</form></div>";
  
}

function addGuestToRosterForm() {

print" <div>";
print" <form action='addGuestToRoster.php' method='post'>";
print"  Guest ID: <input type='text' name='guestID'>";
print"  <input type='submit' value='Add Guest to Roster'>";
print"</form></div>";


}



function showRosterTable($conferenceID) {
 //create db object
    $db = NewADOConnection('mysql');

    //query string

    $conference_name_query = "SELECT name FROM `cosc4359_conference` WHERE conference_id=".$conferenceID."";

    $roster_query = "SELECT DISTINCT guest.guest_id, fname, lname, gender.gender, conference.name, residence_hall_name, room_name, check_in, check_out FROM cosc4359_guest as guest, cosc4359_gender as gender, cosc4359_conference as conference, cosc4359_residence_hall as residence_hall, cosc4359_room as room, cosc4359_roster as roster, cosc4359_key as k WHERE guest.conference_id = conference.conference_id AND guest.room_id=room.room_id AND room.residence_hall_id=residence_hall.residence_hall_id AND guest.guest_id=roster.guest_id AND guest.gender_id=gender.gender_id AND conference.conference_id=".$conferenceID."";   

    
    
//db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $roster_result = $db->Execute($roster_query);
    $conference_name_result = $db->Execute($conference_name_query);

    //db disconnect
    $db->Close();

    if ($roster_result === false) die("failed");

//    print $conference_name;
    print "<div class='table-window-fit'>";
    print "<h2><a id='roster-table'></a>".$conference_name_result->fields[0]." Roster</h2>";
//    print $conference_name->fields[0];
    $table_id.="rostertable";
    $rosterTableString = "<div>\n".
                          "<table id='".$table_id."' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Guest ID</th>\n".
                          "         <th>First Name</th>\n".
                          "         <th>Last Name</th>\n".
                          "         <th>Gender</th>\n".
                          "         <th>Conference Name</th>\n".
                         "         <th>Residence Hall</th>\n".
                          "         <th>Room</th>\n".
                          "         <th>Check-In</th>\n".
                          "         <th>Check-Out</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Guest ID</th>\n".
                        "         <th>First Name</th>\n".
                          "         <th>Last Name</th>\n".
                          "         <th>Gender</th>\n".
                          "         <th>Conference Name</th>\n".
                         "         <th>Residence Hall</th>\n".
                          "         <th>Room</th>\n".
                          "         <th>Check-In</th>\n".
                          "         <th>Check-Out</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with information from database-->\n";

    while (!$roster_result->EOF) {
        for ($i=0, $max=$roster_result->FieldCount(); $i < $max; $i+=9)
            $fieldResultRoster.=  "<tr>\n".
//                                     "<td id=".$roster_result->fields[$i+0].">".$roster_result->fields[$i+1]."</td>\n".
                                     
                                    "<td>".$roster_result->fields[$i+0]."</td>\n".
                                    "<td>".$roster_result->fields[$i+1]."</td>\n".

				     "<td>".$roster_result->fields[$i+2]."</td>\n".
                                     "<td>".$roster_result->fields[$i+3]."</td>\n".
                                     "<td>".$roster_result->fields[$i+4]."</td>\n".
                                     "<td>".$roster_result->fields[$i+5]."</td>\n".
                                     "<td>".$roster_result->fields[$i+6]."</td>\n".
                                     "<td>".$roster_result->fields[$i+7]."</td>\n".
                                     "<td>".$roster_result->fields[$i+8]."</td>\n".
                                  "</tr>\n";
            $roster_result->MoveNext();
        }
    $rosterTableString.= $fieldResultRoster;
    $rosterTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $rosterTableString;
   // getRosterDataTable();

       print" <style type='text/css'>\n";
    print"    .table-window-fit{\n";
    print"    margin: 20px;\n";
    print"    }\n";
    print" </style>\n";

}



//prints javascript for building DataTables object
function getDataTableRoster1() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){\n";
print"       $('#rostertable').dataTable({\n";
print"	               'fnRowCallback': function( nRow, aData, iDisplayIndex ) {\n";
print"    if ( aData[7].valueOf() != '0000-00-00 00:00:00'.valueOf() && aData[8].valueOf() === '0000-00-00 00:00:00'.valueOf()  )  {\n";
print"	           console.log(typeof(aData[7]));\n";
print"			        console.log(nRow);\n";
print"         if(nRow.className == 'odd'){\n"; 
print"	              		    	       $(nRow).addClass( 'success' ).removeClass('odd success').addClass( 'success' )\n";
print"					       			 	   		       			  	       }\n";
print"         if(nRow.className == 'even'){\n";
print"          $(nRow).addClass( 'success' ).removeClass('even success').addClass( 'success' )\n";
print"          }";
print"          }";

print"    if ( aData[8].valueOf() != '0000-00-00 00:00:00'.valueOf() )  {";
print"         console.log(typeof(aData[7]));";
print"         console.log(nRow);";
print"         if(nRow.className == 'odd'){";
print"          $(nRow).addClass( 'danger' ).removeClass('odd danger').addClass( 'danger' )";
print"          }";
print"         if(nRow.className == 'even'){";
print"          $(nRow).addClass( 'danger' ).removeClass('even warning').addClass( 'danger' )";
print"          }";
print"				    }";

print"    if ( aData[8].valueOf() != '0000-00-00 00:00:00'.valueOf() )  {";
print"         console.log(typeof(aData[7]));";
print"         console.log(nRow);";
print"          $(nRow).addClass( 'warning' ).removeClass('odd warning').addClass( 'warning' )";
print"              }";

print"				}";
print"						     });";

print "    $('#rostertable tbody').on( 'click', 'tr', function () {\n";
print "        $(this).toggleClass('active');\n";
print "    } );\n";


print "    $('#rostertable tr').on('click', 'td', function () {\n";
print "        $(this).toggleClass('active');\n";


print "    ar5 =$(this).attr('id');\n";
print "            document.getElementById('roster-guest-selection-input1').value = ar5;\n";
//print "            document.getElementById('roster-guest-selection-input2').value = ar5;\n";

print "        console.log(ar5);\n";

print "    } );";



print"       })";
print"     </script>";

}



function viewConferenceTable($conferenceGroupID) {

  //internal testing print statement
  //  print $conferenceGroupID;

  //create db object
    $db = NewADOConnection('mysql');

    //query string
    $conference_query = "SELECT * FROM cosc4359_conference WHERE conference_group_id='".$conferenceGroupID."'";
    $conference_name_query = "SELECT * FROM cosc4359_conference_group WHERE conference_group_id='".$conferenceGroupID."'";
    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $conference_result = $db->Execute($conference_query);
    $conference_name =  $db->Execute($conference_name_query);
    //db disconnect
    $db->Close();

    if ($conference_result === false) die("failed");
    if ($conference_name_result === false) die("failed");

//    print $conference_name;
    print "<div class='table-window-fit'>";
    print "<h2><a id='conference-table-".$conference_name->fields[0]."'></a>".$conference_name->fields[1]."</h2>";
//    print $conference_name->fields[0];
    $table_id.="conferencetable-".$conference_name->fields[0]."";
    $conferenceTableString = "<div>\n".
                          "<table id='".$table_id."' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
//                          "         <th>Conference ID</th>\n".
//                          "         <th>Roster ID</th>\n".
                          "         <th>Conference Name</th>\n".
//                          "         <th>Conference Group ID</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
//                          "         <th>Conference ID</th>\n".
//                         "         <th>Roster ID</th>\n".
                          "         <th>Conference Name</th>\n".
//                          "         <th>Conference Group ID</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody class='conferencetable'>\n".
                          "   <!-- populating body with information from database-->\n";

    while (!$conference_result->EOF) {
        for ($i=0, $max=$conference_result->FieldCount(); $i < $max; $i+=4)
            $fieldResultConference.=  "<tr>\n".
						       "<td class='conferencename' id=".$conference_result->fields[$i+0].">".$conference_result->fields[$i+1]."</td>\n". 
 //                                    "<td>".$conference_result->fields[$i+0]."</td>\n".
 //                                    "<td>".$conference_result->fields[$i+1]."</td>\n".
 //                                    "<td>".$conference_result->fields[$i+2]."</td>\n".
 //                                    "<td>".$conference_result->fields[$i+3]."</td>\n".
                                  "</tr>\n";
            $conference_result->MoveNext();
        }
    $conferenceTableString.= $fieldResultConference;
    $conferenceTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $conferenceTableString;
    getConferenceDataTable($table_id,$conferenceGroupID);
    showConferenceAddForm($conferenceGroupID);

//return $conferenceTableString;    

}

function showConferenceSelectForm($conferenceGroupID) {

 
 $self = $_SERVER['PHP_SELF'];
 print "<div> <form method='post' action='$self' >\n";
  print "<input type='hidden' id='conference-selection-input".$conferenceGroupID."' name='conferenceselectioninput' value='' />\n";
  print "<input type='submit' name='selectConferenceSubmit' value='View selected conference' />\n";
  print "</form>\n</div>\n";
}



function showConferenceAddForm($conferenceGroupID) {
  $self = $_SERVER['PHP_SELF'];
  print "<br></br>";
  print "<div> <form method='post' action='$self' >\n";
  print "<input type='hidden' name='conferenceGroupID' value='".$conferenceGroupID."' />\n";
  print "<input type='text' name='addConferenceName' value=''>\n";
  print "<input type='submit' name='conferenceAddFormSubmit' value='Add Conference' />\n";
  print "</form>\n</div>\n";
  print "<br></br>";

}



function addConference($conferenceGroupID, $conferenceName){
 //internal testing print statement
  //print "addConferenceGroup(".$conferenceGroupName.")";

  //print $conferenceGroupName;
  $self = $_SERVER['PHP_SELF'];
  $db = NewADOConnection('mysql');
  $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
  $sql = "INSERT INTO cosc4359_conference (name, conference_group_id) VALUES ('". $conferenceName."','".$conferenceGroupID."')";
  $db->Execute($sql);

 if ($result === false) die("failed");
}

//prints javascript for building DataTables object
function getConferenceDataTable($tableID,$conferenceGroupID) {
	  print $tableID;
  //internal testing print statement
  //print "getConferenceDataTable()";
  //print"     <!-- Status Table -->\n";
  
  print "     <script>";
  print"       $(document).ready(function(){";
  print"       $('#".$tableID."').dataTable();";

print "    $('#".$tableID." tbody').on( 'click', 'tr', function () {\n";
print "        $(this).toggleClass('active');\n";
print "    } );\n";


print "    $('#".$tableID." tr').on('click', 'td', function () {";
print "        $(this).toggleClass('active');\n";


print "    ar =$(this).attr('id');";
print "            document.getElementById('conference-selection-input".$conferenceGroupID."').value = ar;";
print "            document.getElementById('conference-add-input".$conferenceGroupID."').value = ar;";

print "        console.log(ar);";

print "    } );";

print "} );\n";
print "</script>\n";

}



function viewGuestListTable($conferenceID) {

  //internal testing print statement
//  print "viewGuestListTable(".$conferenceID.")";
  
    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $guest_query = "SELECT * FROM cosc4359_guest WHERE conference_id='".$conferenceID."'";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $guest_result = $db->Execute($guest_query);

    //db disconnect
    $db->Close();

    if ($guest_result === false) die("failed");

    $guestTableString = "<div>\n".
                          "<table id='guesttable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Guest ID</th>\n".
                          "         <th>Conference ID</th>\n".
                          "         <th>First Name</th>\n".
                          "         <th>Last Name</th>\n".
                          "         <th>Room ID</th>\n".
                          "         <th>Gender ID</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Guest ID</th>\n".
                          "         <th>Conference ID</th>\n".
                          "         <th>First Name</th>\n".
                          "         <th>Last Name</th>\n".
                          "         <th>Room ID</th>\n".
                          "         <th>Gender ID</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with guest information from database-->\n";

    while (!$guest_result->EOF) {
        for ($i=0, $max=$guest_result->FieldCount(); $i < $max; $i+=6)
            $fieldResultGuest.=  "<tr>\n".
                                     "<td>".$guest_result->fields[$i+0]."</td>\n".
                                     "<td>".$guest_result->fields[$i+1]."</td>\n".
                                     "<td>".$guest_result->fields[$i+2]."</td>\n".
                                     "<td>".$guest_result->fields[$i+3]."</td>\n".
                                     "<td>".$guest_result->fields[$i+4]."</td>\n".
                                     "<td>".$guest_result->fields[$i+5]."</td>\n".
                                  "</tr>\n";
            $guest_result->MoveNext();
        }
    $guestTableString.= $fieldResultGuest;
    $guestTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $guestTableString;
}

function getGuestListDataTable() {

  //internal testing print statement
//  print "getGuestListDataTable()";
  
  print"     <!-- Status Table -->";
  print"     <script>";
  print"       $(document).ready(function(){";
  print"       $('#guesttable').dataTable();";
  print"       })";
  print"     </script>";

}

function showGuestSelectForm($conferenceID) {

  $self = $_SERVER['PHP_SELF'];
  $db = NewADOConnection('mysql');
  $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
  $conference_query = "SELECT * FROM cosc4359_guest WHERE conference_id='".$conferenceID."'";
  $result = $db->Execute($conference_query);

 if ($result === false) die("failed");

  print "<div> <form method='post' action='$self' >\n";
  print "<label for='guest-option'>Select Guest (select one):</label>\n";
  print "      <select class='form-control' id='guest-option' name='guest-option'>\n";
  foreach($result as $k => $row) {

           echo "<option value='".$row[0]."'>".$row[2]." ".$row[3]."</option>";
  }
  print"    </select>\n";
  print "<input type='submit' name='selectGuestSubmit' ".
        " value='View selected guest' />\n";
 print "</form>\n</div>\n";
 print "<br></br>";

}

function showCheckInForm() {

  print "<div> <form method='post' action='$self' >\n";
  print "<input type='hidden' name='conferenceselectioninput' value='".$_POST['conferenceselectioninput']."' />\n";
  print "<input type='hidden' id='roster-guest-selection-input1' name='rosterguestid' value='' />\n";
  print "<input type='submit' name='selectRosterGuestCheckInSubmit' value='Check-In ' />\n";
  print "</form>\n</div>\n";
  print "<br></br>";
}

function showCheckOutForm() {

  print "<div> <form method='post' action='$self' >\n";
  print "<input type='hidden' name='conferenceselectioninput' value='".$_POST['conferenceselectioninput']."' />\n";
  print "<input type='hidden' id='roster-guest-selection-input2' name='rosterguestid' value='' />\n";
  print "<input type='submit' name='selectRosterGuestCheckOutSubmit' value='Check-Out ' />\n";
  print "</form>\n</div>\n";
  print "<br></br>";

}
//

//

//

//

function viewResidenceHallTable($residenceHallID) {

  //create db object
    $db = NewADOConnection('mysql');

    //query string
    //$conference_query = "SELECT * FROM cosc4359_conference WHERE conference_group_id='".$conferenceGroupID."'";
    $residence_hall_query = "SELECT room.room_id, rh.residence_hall_id, rh.residence_hall_name, room.room_name FROM cosc4359_room as room, cosc4359_residence_hall as rh WHERE room.residence_hall_id = rh.residence_hall_id AND room.residence_hall_id='".$residenceHallID."'";
    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    //$conference_result = $db->Execute($conference_query);
    $residence_hall_result =  $db->Execute($residence_hall_query);
    //db disconnect
    $db->Close();

    //if ($conference_result === false) die("failed");
    if ($residence_hall_result === false) die("failed");

//    print $conference_name;
    print "<div class='table-window-fit'>";
    print "<h2><a id='Residence Hall: ".$residence_hall_result->fields[1]."'></a>".$conference_name->fields[1]."</h2>";
//    print $conference_name->fields[0];
    $rhTableString = "<div>\n".
                          "<table id='residencehall' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Room ID</th>\n".
                         "         <th>Residence Hall ID</th>\n".
                          "         <th>Residence Hall Name</th>\n".
                          "         <th>Room Name</th>\n".
//                          "         <th>Conference Group ID</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                         "         <th>Room ID</th>\n".
                       "         <th>Residence Hall ID</th>\n".

                          "         <th>Residence Hall Name</th>\n".
                          "         <th>Room Name</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody class='residencehalltable'>\n".
                          "   <!-- populating body with information from database-->\n";

    while (!$residence_hall_result->EOF) {
        for ($i=0, $max=$residence_hall_result->FieldCount(); $i < $max; $i+=4)
            $fieldResultRH.=  "<tr>\n".
//								       "<td class='roomid' id=".$residence_hall_result->fields[$i+0].">".$residence_hall_result->fields[$i+1]."</td>\n". 
                                     "<td>".$residence_hall_result->fields[$i+0]."</td>\n".
                                     "<td>".$residence_hall_result->fields[$i+1]."</td>\n".
                                     "<td>".$residence_hall_result->fields[$i+2]."</td>\n".
                                     "<td>".$residence_hall_result->fields[$i+3]."</td>\n".

 //                                    "<td>".$conference_result->fields[$i+3]."</td>\n".
                                  "</tr>\n";
            $residence_hall_result->MoveNext();
        }
    $rhTableString.= $fieldResultRH;
    $rhTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $rhTableString;
    getrhDataTable();

}


//prints javascript for building DataTables object
function getrhDataTable() {
  //internal testing print statement
  //print "getConferenceDataTable()";
  //print"     <!-- Status Table -->\n";
  
  print "     <script>";
  print"       $(document).ready(function(){";
  print"       $('#residencehall').dataTable();";

print "    $('#".$tableID." tbody').on( 'click', 'tr', function () {\n";
print "        $(this).toggleClass('active');\n";
print "    } );\n";


print "    $('#".$tableID." tr').on('click', 'td', function () {";
print "        $(this).toggleClass('active');\n";

/*
print "    ar =$(this).attr('id');";
print "            document.getElementById('conference-selection-input".$conferenceGroupID."').value = ar;";
print "            document.getElementById('conference-add-input".$conferenceGroupID."').value = ar;";

print "        console.log(ar);";
*/
print "    } );";

print "} );\n";
print "</script>\n";

}



?>