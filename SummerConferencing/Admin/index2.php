<?php
//include("../../adodb5/adodb.inc.php");
include("./admin_api.php");

printDocHeading();
print" <body>\n";
printNavBar();
if(empty ($_POST))
{
printConferenceGroupTable();
//getDataTableConferenceGroup();
showConferenceGroupAddForm();
//print "showConferenceGroupSelectForm()\n";
showConferenceGroupSelectForm();
print" showResidenceHallTable();\n";
print" showAddResidenceHallForm();\n";
print" showOccupiedRoomTable();\n";
print" showRoomsWithMaintenanceNeeded()\n";

}
else if ($_POST['conferenceGroupAddFormSubmit'])
{
//print $_POST['add-conference-group-name'];
addConferenceGroup($_POST['add-conference-group-name']);
printConferenceGroupTable();
getDataTableConferenceGroup();
showConferenceGroupAddForm();
print "showConferenceGroupSelectForm()\n";
showConferenceGroupSelectForm();

}
else if ($_POST['selectConferenceGroupSubmit'])
{
print "conference id: ";
print $_POST['conference-group-option'];
print "<br></br>\n";
showConferenceGroupSelectForm();
print" showCheckInTable()\n";
print"<br></br>\n";
print" showAddGuestForm()\n";
print"<br></br>\n";
print" showEditGuestForm()\n";
print"<br></br>\n";
print" showCheckInGuestForm()\n";
print"<br></br>\n";
print" showCheckOutGuestForm()\n";
print"<br></br>\n";
print" showMissingKeyForm()\n";
print"<br></br>\n";
//print" goBackToConferenceGroupSelect()\n";
//print" <a href='javascript:location.reload(true)'>Refresh this page</a>\n";
}
else if ($_POST['addGuestSubmit'])
{
print" addGuest($conferenceid,$guestfname,$guestlname,$roomid,$genderid)\n";
print" showCheckInTable()\n";
print" showAddGuestForm()\n";
print" showEditGuestSelectForm()\n";
print" showCheckInGuestForm()\n";
print" showCheckOutGuestForm()\n";
print" showMissingKeyForm()\n";
}
else if ($_POST['editGuestSelectSubmit'])
{
print" showCheckInTable()\n";
print" showEditGuestForm($guestid)\n";
print" undoEditFormSelection($guestid)\n";
}
else if ($_POST['checkInSubmit'])
{
print" checkIn($guestid)\n";
print" showCheckInTable()\n";
print" showAddGuestForm()\n";
print" showEditGuestForm()\n";
print" showCheckInGuestForm()\n";
print" showCheckOutGuestForm()\n";
print" showMissingKeyForm()\n";
}
else if ($_POST['checkOutSubmit'])
{
print" checkOut($guestid)\n";
print" showCheckInTable()\n";
print" showAddGuestForm()\n";
print" showEditGuestForm()\n";
print" showCheckInGuestForm()\n";
print" showCheckOutGuestForm()\n";
print" showMissingKeyForm()\n";
}
else if ($_POST['missingKeySubmit']='Lost')
{
print" lostKey($guestid)\n";
print" showCheckInTable()\n";
print" showAddGuestForm()\n";
print" showEditGuestForm()\n";
print" showCheckInGuestForm()\n";
print" showCheckOutGuestForm()\n";
print" showMissingKeyForm()\n";
}
else if ($_POST['missingKeySubmit']='Found')
{
print" FoundKey($guestid)\n";
print" showCheckInTable()\n";
print" showAddGuestForm()\n";
print" showEditGuestForm()\n";
print" showCheckInGuestForm()\n";
print" showCheckOutGuestForm()\n";
print" showMissingKeyForm()\n";
}

function addConferenceGroup($conferenceGroupName) {
  print $conferenceGroupName;
  $self = $_SERVER['PHP_SELF'];
  $db = NewADOConnection('mysql');
  $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
  $sql = "INSERT INTO cosc4359_conference_group (conference_group_name) VALUES ('". $conferenceGroupName."')";
  $db->Execute($sql);

 if ($result === false) die("failed");

}

function showConferenceGroupAddForm() {

 $self = $_SERVER['PHP_SELF'];
  
  print "<div> <form method='post' action='$self' >\n";
  print "<label for='conference-group-option'>Name of Conference Group: </label>\n";
  print "Name of Conference Group: <input type='text' name='add-conference-group-name' value=''><br>\n";
   print "<h3> <input type='submit' name='conferenceGroupAddFormSubmit' ".
        " value='Add Conference Group Form' /> </h3>\n";
  print "</form>\n</div>\n";

}

function getDataTableConferenceGroup() {
print "<script>\n";
print "$(document).ready(function() {\n";
print "    var table = $('#conferencegrouptable').DataTable();\n";
print "    var tt = new $.fn.dataTable.TableTools( table );\n";

print "    $( tt.fnContainer() ).insertBefore('div.dataTables_wrapper');\n";

print "    $('#conferencegrouptable tbody').on( 'click', 'tr', function () {\n";
print "        $(this).toggleClass('active');\n";
print "    } );\n";

print "    $('#button').click( function () {\n";
print "  $('.selected .conferencegroupname').each(function () {\n";
print "        var ar = this.id;\n";
print "        console.log(ar);\n";
print "    });\n";

print "    } );\n";
print "} );\n";
print "</script>\n";
}

function showConferenceGroupSelectForm() {

  $self = $_SERVER['PHP_SELF'];
  $db = NewADOConnection('mysql');
  $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
  $result = $db->Execute("SELECT * FROM cosc4359_conference_group");

 if ($result === false) die("failed");

  print "<div> <form method='post' action='$self' >\n";
  print "<label for='conference-group-option'>Select Conference Group (select one):</label>\n";
  print "      <select class='form-control' id='conference-group-option' name='conference-group-option'>\n";
  foreach($result as $k => $row) {

           echo "<option value='".$row[0]."'>".$row[1]."</option>";
  }
  print"    </select>\n";
  print "<h3> <input type='submit' name='selectConferenceGroupSubmit' ".
        " value='submit conference group selection' /> </h3>\n";
  print "</form>\n</div>\n";

}

function printConferenceGroupTable() {
    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $conference_group_query = "SELECT * FROM cosc4359_conference_group";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $conference_group_result = $db->Execute($conference_group_query);

    //db disconnect
    $db->Close();

    if ($conference_group_result === false) die("failed");

    $conferenceGroupTableString = "<div class='table-window-fit'>\n".
                          "<table id='conferencegrouptable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
//                          "         <th>Conference GroupID</th>\n".
                          "         <th>Conference Group</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
//                          "         <th>Conference Group ID</th>\n".
                          "         <th>Conference Group</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with information from database-->\n";

    while (!$conference_group_result->EOF) {
        for ($i=0, $max=$conference_group_result->FieldCount(); $i < $max; $i+=4)
            $fieldResultConferenceGroup.=  "<tr>\n".
//                                     "<td>".$conference_group_result->fields[$i+0]."</td>\n".
                                     "<td class='conferencegroupname' id=".$conference_group_result->fields[$i+0].">".$conference_group_result->fields[$i+1]."</td>\n".
                                  "</tr>\n";
            $conference_group_result->MoveNext();
        }
    $conferenceGroupTableString.= $fieldResultConferenceGroup;
    $conferenceGroupTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $conferenceGroupTableString;
    print" <style type='text/css'>";
    print"    .table-window-fit{";
    print"    margin: 20px;";
    print"    }";
    print" </style>";


}

//prints javascript for building DataTables object
/*
function getDataTableConferenceGroup() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#conferencegrouptable').dataTable();";
print"       })";
print"     </script>";


}
*/

function printDocHeading() {
print" <!DOCTYPE html>\n";
print" <html lang='en'\n>";
print" <head>\n";
print" <meta charset='UTF-8'>\n";
print" <title>SEU Summer Conferencing Admin</title>\n";
print" <link rel='stylesheet' href='../bootstrap-3.3.2/dist/css/bootstrap.min.css'>\n";

print" <link rel='stylesheet' type='text/css' href='../TableTools-2.2.3/css/dataTables.tableTools.css'>";

print" <link rel='stylesheet' href='../bootstrap-3.3.2/dist/css/bootstrap-theme.min.css'>\n";
print" <link rel='stylesheet' href='../bootstrap-3.3.2/dist/css/bootstrap.custom.css'>\n";
print" <link rel='stylesheet' type='text/css' href='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css'>";

print" <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>\n";
print" <script src='../bootstrap-3.3.2/dist/js/bootstrap.min.js'></script>\n";

print" </style>\n";
print" </head>\n";
}

function printNavBar() {
print" <div class='bs-example'>\n";
print"    <nav role='navigation' class='navbar navbar-custom'>\n";
print"        <!-- Brand and toggle get grouped for better mobile display -->\n";
print"        <div class='navbar-header'>\n";
print"            <button type='button' data-target='#navbarCollapse' data-toggle='collapse' class='navbar-toggle'>\n";
print"                <span class='sr-only'>Toggle navigation</span>\n";
print"                <span class='icon-bar'></span>\n";
print"                <span class='icon-bar'></span>\n";
print"                <span class='icon-bar'></span>\n";
print"            </button>\n";
print"            <a href='http://www.cs.stedwards.edu/~mharper5/SummerConferencing/index2.php' class='navbar-brand'>Home</a>\n";
print"        </div>\n";
print"        <!-- Collection of nav links, forms, and other content for toggling -->\n";
print"        <div id='navbarCollapse' class='collapse navbar-collapse'>\n";
print"            <ul class='nav navbar-nav'>\n";
/*
print"                <li class='active'><a href='#'>Conferences</a></li>\n";
print"                <li><a href='#'>Residence Halls</a></li>\n";
print"                <li class='dropdown'>\n";
print"                    <a data-toggle='dropdown' class='dropdown-toggle' href='#'>View Reports <b class='caret'></b></a>\n";
print"                    <ul role='menu' class='dropdown-menu'>\n";
print"                        <li><a href='#'>Guests On Campus</a></li>\n";
print"                        <li class='divider'></li>\n";
print"                        <li><a href='#'>Maintenance</a></li>\n";
print"                    </ul>\n";
print"                </li>\n";
print"            </ul>\n";
print"            <form role='search' class='navbar-form navbar-left'>\n";
print"                <div class='form-group'>\n";
print"                    <input type='text' placeholder='Find Guest' class='form-control'>\n";
print"                </div>\n";
print"            </form>\n";
print"            <ul class='nav navbar-nav navbar-right'>\n";
print"                <li><a href='#'>Login</a></li>\n";
*/
print"            </ul>\n";
print"        </div>\n";
print"    </nav>\n";
print" </div>\n";

}

print"     <!-- JQuery -->";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.10.5/jquery.dataTables.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>";
print"  <script type='text/javascript' charset='utf8' src='../TableTools-2.2.3/js/dataTables.tableTools.js'></script>\n";



print" </body>\n";
print" </html>\n";						
?>