CREATE TABLE cosc4359_roster(
roster_id INT(7) NOT NULL AUTO_INCREMENT,
guest_id INT(7),
check_in TIMESTAMP,
check_out TIMESTAMP,
PRIMARY KEY (roster_id)
) ENGINE=InnoDB; 
