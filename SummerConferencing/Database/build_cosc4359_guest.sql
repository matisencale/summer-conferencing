CREATE TABLE cosc4359_guest(
guest_id INT(7) NOT NULL AUTO_INCREMENT,
conference_id INT(7),
fname VARCHAR(40),
lname VARCHAR(40),
room_id INT(7),
gender_id INT(1),
PRIMARY KEY (guest_id),
FOREIGN KEY (conference_id), REFERENCES cosc4359_conference(conference_id),
FOREIGN KEY (room_id), REFERENCES cosc4359_room(room_id),
FOREIGN KEY (gender_id), REFERENCES cosc4359_gender(gender_id)
) ENGINE=InnoDB; 
