CREATE TABLE cosc4359_conference(
conference_id INT(7) NOT NULL AUTO_INCREMENT,
roster_id INT(7),
name VARCHAR(40), 
conference_group_id INT(7),
PRIMARY KEY (conference_id)
) ENGINE=InnoDB; 
