CREATE TABLE cosc4359_residence_hall(
residence_hall_id INT(7) NOT NULL AUTO_INCREMENT,
status_id INT(7),
single_rate DECIMAL(6,2),
double_rate DECIMAL(6,2),
PRIMARY KEY (residence_hall_id),
FOREIGN KEY (status_id), REFERENCES cosc4359_status(status_id)
) ENGINE=InnoDB; 
