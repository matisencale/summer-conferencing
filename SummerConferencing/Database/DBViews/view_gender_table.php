<?php
include("../../../adodb5/adodb.inc.php");

print"<html>";
print"<head>";
//print"  <link rel='stylesheet' type='text/css' href='http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.10.5/css/jquery.dataTables.css'>";
print"  <link rel='stylesheet' type='text/css' href='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css'>";
print"<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>";
print"</head>";
print"  <body>";
print"     <h1>Gender Table</h1>";

printGenderTable();


print"     <h1>Status Table</h1>";

printStatusTable();

print"     <h1>Conference Table</h1>";

printConferenceTable();

print"     <h1>Conference Group Table</h1>";

printConferenceGroupTable();

print"     <h1>Guest Table</h1>";


printGuestTable();

print"     <h1>Key Table</h1>";

printKeyTable();

print"     <h1>Residence Hall Table</h1>";

printResidenceHallTable();

print"     <h1>Room Table</h1>";

printRoomTable();

print"     <h1>Roster Table</h1>";

printRosterTable();

print"     <!-- JQuery -->";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.10.5/jquery.dataTables.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>";

     
print"     <!-- Build Tables -->";

//javascript for datatable object
getDataTableGender();
getDataTableStatus();
getDataTableConference();
getDataTableConferenceGroup();
getDataTableGuest();
getDataTableKey();
getDataTableResidenceHall();
getDataTableRoom();
getDataTableRoster();

print"  </body>";
print"</html>";

//printGenderTable()

function printGenderTable() {
    
    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $gender_query = "SELECT * FROM cosc4359_gender";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
    
    //query results
    $gender_result = $db->Execute($gender_query);
    
    //db disconnect
    $db->Close();

    if ($gender_result === false) die("failed");

    $genderTableString = "<div>\n".
                          "<table id='gendertable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Gender ID</th>\n".
                          "         <th>Gender</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Gender ID</th>\n".
                          "         <th>Gender</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".
   
                          "   <tbody>\n".
                          "   <!-- populating body with guest information from database-->\n";

    while (!$gender_result->EOF) {
        for ($i=0, $max=$gender_result->FieldCount(); $i < $max; $i+=2)
            $fieldResultGender.=  "<tr>\n".
                                     "<td>".$gender_result->fields[$i+0]."</td>\n".
                                     "<td>".$gender_result->fields[$i+1]."</td>\n".
                                  "</tr>\n";
            $gender_result->MoveNext();
    	}
    $genderTableString.= $fieldResultGender;
    $genderTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $genderTableString;
}

//prints javascript for building DataTables object 
function getDataTableGender() {

print"     <!-- Gender Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#gendertable').dataTable();";
print"       })";
print"     </script>";

}


//printStatusTable()

function printStatusTable() {

    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $status_query = "SELECT * FROM cosc4359_status";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $status_result = $db->Execute($status_query);

    //db disconnect
    $db->Close();

    if ($status_result === false) die("failed");

    $statusTableString = "<div>\n".
                          "<table id='statustable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Status ID</th>\n".
                          "         <th>Status</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Status ID</th>\n".
                          "         <th>Status</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with information from database-->\n";

    while (!$status_result->EOF) {
        for ($i=0, $max=$status_result->FieldCount(); $i < $max; $i+=2)
            $fieldResultStatus.=  "<tr>\n".
                                     "<td>".$status_result->fields[$i+0]."</td>\n".
                                     "<td>".$status_result->fields[$i+1]."</td>\n".
                                  "</tr>\n";
            $status_result->MoveNext();
        }
    $statusTableString.= $fieldResultStatus;
    $statusTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $statusTableString;

}

//prints javascript for building DataTables object
function getDataTableStatus() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#statustable').dataTable();";
print"       })";
print"     </script>";

}

function printConferenceTable() {

    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $conference_query = "SELECT * FROM cosc4359_conference";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $conference_result = $db->Execute($conference_query);

    //db disconnect
    $db->Close();

    if ($conference_result === false) die("failed");

    $conferenceTableString = "<div>\n".
                          "<table id='conferencetable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Conference ID</th>\n".
                          "         <th>Roster ID</th>\n".
                          "         <th>Conference Name</th>\n".
                          "         <th>Conference Group ID</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Conference ID</th>\n".
                          "         <th>Roster ID</th>\n".
                          "         <th>Conference Name</th>\n".
                          "         <th>Conference Group ID</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with information from database-->\n";

    while (!$conference_result->EOF) {
        for ($i=0, $max=$conference_result->FieldCount(); $i < $max; $i+=4)
            $fieldResultConference.=  "<tr>\n".
                                     "<td>".$conference_result->fields[$i+0]."</td>\n".
                                     "<td>".$conference_result->fields[$i+1]."</td>\n".
                                     "<td>".$conference_result->fields[$i+2]."</td>\n".
                                     "<td>".$conference_result->fields[$i+3]."</td>\n".
                                  "</tr>\n";
            $conference_result->MoveNext();
        }
    $conferenceTableString.= $fieldResultConference;
    $conferenceTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $conferenceTableString;

}

//prints javascript for building DataTables object
function getDataTableConference() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#conferencetable').dataTable();";
print"       })";
print"     </script>";

}

function printConferenceGroupTable() {
    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $conference_group_query = "SELECT * FROM cosc4359_conference_group";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $conference_group_result = $db->Execute($conference_group_query);

    //db disconnect
    $db->Close();

    if ($conference_group_result === false) die("failed");

    $conferenceGroupTableString = "<div>\n".
                          "<table id='conferencegrouptable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Conference GroupID</th>\n".
                          "         <th>Conference Group Name</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Conference Group ID</th>\n".
                          "         <th>Conference Group Name</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with information from database-->\n";

    while (!$conference_group_result->EOF) {
        for ($i=0, $max=$conference_group_result->FieldCount(); $i < $max; $i+=4)
            $fieldResultConferenceGroup.=  "<tr>\n".
                                     "<td>".$conference_group_result->fields[$i+0]."</td>\n".
                                     "<td>".$conference_group_result->fields[$i+1]."</td>\n".
                                  "</tr>\n";
            $conference_group_result->MoveNext();
        }
    $conferenceGroupTableString.= $fieldResultConferenceGroup;
    $conferenceGroupTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $conferenceGroupTableString;

}

//prints javascript for building DataTables object
function getDataTableConferenceGroup() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#conferencegrouptable').dataTable();";
print"       })";
print"     </script>";

}

function printGuestTable() {

    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $guest_query = "SELECT * FROM cosc4359_guest";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $guest_result = $db->Execute($guest_query);

    //db disconnect
    $db->Close();

    if ($guest_result === false) die("failed");

    $guestTableString = "<div>\n".
                          "<table id='guesttable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Guest ID</th>\n".
                          "         <th>Conference ID</th>\n".
                          "         <th>First Name</th>\n".
                          "         <th>Last Name</th>\n".
                          "         <th>Room ID</th>\n".
                          "         <th>Gender ID</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Guest ID</th>\n".
                          "         <th>Conference ID</th>\n".
                          "         <th>First Name</th>\n".
                          "         <th>Last Name</th>\n".
                          "         <th>Room ID</th>\n".
                          "         <th>Gender ID</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with guest information from database-->\n";

    while (!$guest_result->EOF) {
        for ($i=0, $max=$guest_result->FieldCount(); $i < $max; $i+=6)
            $fieldResultGuest.=  "<tr>\n".
                                     "<td>".$guest_result->fields[$i+0]."</td>\n".
                                     "<td>".$guest_result->fields[$i+1]."</td>\n".
                                     "<td>".$guest_result->fields[$i+2]."</td>\n".
                                     "<td>".$guest_result->fields[$i+3]."</td>\n".
                                     "<td>".$guest_result->fields[$i+4]."</td>\n".
                                     "<td>".$guest_result->fields[$i+5]."</td>\n".
                                  "</tr>\n";
            $guest_result->MoveNext();
        }
    $guestTableString.= $fieldResultGuest;
    $guestTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $guestTableString;

}

//prints javascript for building DataTables object
function getDataTableGuest() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#guesttable').dataTable();";
print"       })";
print"     </script>";

}

function printKeyTable() {

    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $key_query = "SELECT * FROM cosc4359_key";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $key_result = $db->Execute($key_query);

    //db disconnect
    $db->Close();

    if ($key_result === false) die("failed");

    $keyTableString = "<div>\n".
                          "<table id='keytable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Key ID</th>\n".
                          "         <th>Key Number</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Key ID</th>\n".
                          "         <th>Key Number</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with guest information from database-->\n";

    while (!$key_result->EOF) {
        for ($i=0, $max=$key_result->FieldCount(); $i < $max; $i+=2)
            $fieldResultKey.=  "<tr>\n".
                                     "<td>".$key_result->fields[$i+0]."</td>\n".
                                     "<td>".$key_result->fields[$i+1]."</td>\n".
                                  "</tr>\n";
            $key_result->MoveNext();
        }
    $keyTableString.= $fieldResultKey;
    $keyTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $keyTableString;

}

//prints javascript for building DataTables object
function getDataTableKey() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#keytable').dataTable();";
print"       })";
print"     </script>";

}

function printResidenceHallTable() {

    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $residence_hall_query = "SELECT * FROM cosc4359_residence_hall";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $residence_hall_result = $db->Execute($residence_hall_query);

    //db disconnect
    $db->Close();

    if ($residence_hall_result === false) die("failed");

    $residenceHallTableString = "<div>\n".
                          "<table id='residencehalltable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Residence Hall ID</th>\n".
                          "         <th>Status ID</th>\n".
                          "         <th>Single Rate</th>\n".
                          "         <th>Double Rate</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Residence Hall ID</th>\n".
                          "         <th>Status ID</th>\n".
                          "         <th>Single Rate</th>\n".
                          "         <th>Double Rate</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with guest information from database-->\n";

    while (!$residence_hall_result->EOF) {
        for ($i=0, $max=$residence_hall_result->FieldCount(); $i < $max; $i+=4)
            $fieldResultResidenceHall.=  "<tr>\n".
                                     "<td>".$residence_hall_result->fields[$i+0]."</td>\n".
                                     "<td>".$residence_hall_result->fields[$i+1]."</td>\n".
                                     "<td>".$residence_hall_result->fields[$i+2]."</td>\n".
                                     "<td>".$residence_hall_result->fields[$i+3]."</td>\n".
                                  "</tr>\n";
            $residence_hall_result->MoveNext();
        }
    $residenceHallTableString.= $fieldResultResidenceHall;
    $residenceHallTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $residenceHallTableString;

}

//prints javascript for building DataTables object
function getDataTableResidenceHall() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#residencehalltable').dataTable();";
print"       })";
print"     </script>";

}

function printRoomTable() {

    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $room_query = "SELECT * FROM cosc4359_room";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $room_result = $db->Execute($room_query);

    //db disconnect
    $db->Close();

    if ($room_result === false) die("failed");

    $roomTableString = "<div>\n".
                          "<table id='roomtable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Room ID</th>\n".
                          "         <th>Residence Hall ID</th>\n".
                          "         <th>Status ID</th>\n".
                          "         <th>Key ID</th>\n".
                          "         <th>Room Name</th>\n".
                          "         <th>Suite</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Room ID</th>\n".
                          "         <th>Residence Hall ID</th>\n".
                          "         <th>Status ID</th>\n".
                          "         <th>Key ID</th>\n".
                          "         <th>Room Name</th>\n".
                          "         <th>Suite</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with guest information from database-->\n";

    while (!$room_result->EOF) {
        for ($i=0, $max=$room_result->FieldCount(); $i < $max; $i+=6)
            $fieldResultRoom.=  "<tr>\n".
                                     "<td>".$room_result->fields[$i+0]."</td>\n".
                                     "<td>".$room_result->fields[$i+1]."</td>\n".
                                     "<td>".$room_result->fields[$i+2]."</td>\n".
                                     "<td>".$room_result->fields[$i+3]."</td>\n".
                                     "<td>".$room_result->fields[$i+4]."</td>\n".
                                     "<td>".$room_result->fields[$i+5]."</td>\n".
			          "</tr>\n";
            $room_result->MoveNext();
        }
    $roomTableString.= $fieldResultRoom;
    $roomTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $roomTableString;

}

//prints javascript for building DataTables object
function getDataTableRoom() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#roomtable').dataTable();";
print"       })";
print"     </script>";

}
/*
function getAllAvailableRoomsOnCampus() {

}

function getAllAvailableRoomsFromResidenceHall() {

}

function setRoomStatus() {

}

*/

function printRosterTable() {

    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $roster_query = "SELECT * FROM cosc4359_roster";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $roster_result = $db->Execute($roster_query);

    //db disconnect
    $db->Close();

    if ($roster_result === false) die("failed");

    $rosterTableString = "<div>\n".
                          "<table id='rostertable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>Roster ID</th>\n".
                          "         <th>Guest ID</th>\n".
                          "         <th>Check In Time</th>\n".
                          "         <th>Check Out Time</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>Roster ID</th>\n".
                          "         <th>Guest ID</th>\n".
                          "         <th>Check In Time</th>\n".
                          "         <th>Check Out Time</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with guest information from database-->\n";

    while (!$roster_result->EOF) {
        for ($i=0, $max=$roster_result->FieldCount(); $i < $max; $i+=4)
          $fieldResultRoster.=  "<tr>\n".
                                     "<td >".$roster_result->fields[$i+0]."</td>\n".
                                     "<td>".$roster_result->fields[$i+1]."</td>\n".
                                     "<td id='check-in'>".$roster_result->fields[$i+2]."</td>\n".
                                     "<td>".$roster_result->fields[$i+3]."</td>\n".
                                  "</tr>\n";
            $roster_result->MoveNext();
        }
    $rosterTableString.= $fieldResultRoster;
    $rosterTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $rosterTableString;

}

//prints javascript for building DataTables object
function getDataTableRoster() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#rostertable').dataTable({";
print"	     'fnRowCallback': function( nRow, aData, iDisplayIndex ) {";
print"    if ( aData[2].valueOf() != '0000-00-00 00:00:00'.valueOf() && aData[3].valueOf() === '0000-00-00 00:00:00'.valueOf()  )  {";
print"	       console.log(typeof(aData[2]));";
print"	       console.log(nRow);";
print"         if(nRow.className == 'odd'){"; 
print"		$(nRow).addClass( 'success' ).removeClass('odd success').addClass( 'success' )";
print"		}";
print"         if(nRow.className == 'even'){";
print"          $(nRow).addClass( 'success' ).removeClass('even success').addClass( 'success' )";
print"          }";
print"          }";

print"    if ( aData[3].valueOf() != '0000-00-00 00:00:00'.valueOf() )  {";
print"         console.log(typeof(aData[2]));";
print"         console.log(nRow);";
print"         if(nRow.className == 'odd'){";
print"          $(nRow).addClass( 'danger' ).removeClass('odd danger').addClass( 'danger' )";
print"          }";
print"         if(nRow.className == 'even'){";
print"          $(nRow).addClass( 'danger' ).removeClass('even warning').addClass( 'danger' )";
print"          }";
print"		    }";

print"    if ( aData[3].valueOf() != '0000-00-00 00:00:00'.valueOf() )  {";
print"         console.log(typeof(aData[2]));";
print"         console.log(nRow);";
print"          $(nRow).addClass( 'warning' ).removeClass('odd warning').addClass( 'warning' )";
print"              }";

print"		}";
print"	     });";
print"       })";
print"     </script>";

}
/*
function rosterCheckIn() {
  
   $db = NewADOConnection('mysql');
   
   $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
   
   $check_in_query="UPDATE `cosc4359_roster` SET check_in = current_timestamp where guest_id=".$_POST['guest_id'];
 
   //echo $query;
   $result = $db->Execute($query);

   if ($result === false) die("failed");

header( 'Location: http://www.cs.stedwards.edu/~mharper5/cosc4157/demo_11_10_14.php' ) ;
*/


?>
