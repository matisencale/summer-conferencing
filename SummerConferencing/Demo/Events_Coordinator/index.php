<?php

require('../myUtils.php');
require('../myAPI.php');

printDocHeadingAdmin();
$template=<<<EOT
<div class="container">

    <p class="lead">Events Coordinator Home</p>
    <p>&emsp;Residence Halls tab used to add residence halls to the system</p>
    <p>&emsp;Conferences tab used to add conference groups, individual conferences and import guest lists</p>
    <p>&emsp;Reports tab used to view reports and generate conference group invoice</p>
    <hr class="half-rule">
    
    <a href="http://www.cs.stedwards.edu/~mharper5/SummerConferencing/Admin/import.php">Guest List Importer</a>
    <hr class="half-rule">

    <a href="http://www.cs.stedwards.edu/~mharper5/SummerConferencing/Admin/import2.php">Residence Hall Importer</a>
    <hr class="half-rule">

    <a href="http://www.cs.stedwards.edu/~mharper5/SummerConferencing/Demo/Events_Coordinator/AutoRoomAssign/">Residence Hall Importer</a>
    <hr class="half-rule">

</div>
EOT;
echo $template;

printFooter();

?>