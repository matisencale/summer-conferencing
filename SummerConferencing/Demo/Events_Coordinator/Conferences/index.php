<?php

require('../../../Admin/admin_api3.php');
require('../../myUtils.php');

$header = <<<EOH
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

 <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js'></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.10.5/jquery.dataTables.min.js'></script>
  <script type='text/javascript' charset='utf8' src='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>
  <script type='text/javascript' charset='utf8' src='../../../TableTools-2.2.3/js/dataTables.tableTools.js'></script>

<title>

  Summer Conference Demo

</title>

<!-- Bootstrap core CSS -->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<link rel='stylesheet' type='text/css' href='../TableTools-2.2.3/css/dataTables.tableTools.css'>

<!-- Favicons -->
<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<link rel="icon" href="/favicon.ico">

  </head>
  <body class="bs-docs-home">
    <a id="skippy" class="sr-only sr-only-focusable" href="#content"><div class="container"><span class="skiplink-text">Skip to main content</span></div></a>

    <!-- Docs master nav -->
    <header class="navbar navbar-static-top bs-docs-nav" id="top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <a href="../" class="navbar-brand">Home</a>
    </div>
  </div>
</header>
EOH;
echo $header;



if(empty ($_POST))
{
viewConferenceGroupTable();
getConferenceGroupDataTable();
viewConferenceTables();
print"<br></br>";
}
else if($_POST['conferenceGroupAddFormSubmit'])
{
viewConferenceTables();
print"<br></br>";
}
printFooter();


?>