<?php

require('./myUtils.php');

printDocHeadingHome();

$template = <<<EOT
<div class="bs-docs-featurette">
  <div class="container">
    <h2 class="bs-docs-featurette-title">Summer Conference Automated Check-In and Invoice Generator Role Functionality</h2>
    <hr class="half-rule">

    <p class="lead">Events Coordinator: Setup System, View Reports, and Generate Invoice</p>
    <p>&emsp;Add|Edit|Delete -  Residence Halls, Conference Groups, Guest Lists, Room Assignments</p>
    <p>&emsp;Residence Hall CSV Format: Residence Hall ID, Room Name, Max Occupancy</p>
    <p>&emsp;Guest List CSV Format: Residence Hall ID, First Name, Last Name, Gender ID</p> 
    <hr class="half-rule">

    <p class="lead">GRA: Overnight Guest Tracking</p>
    <p>&emsp;Check-In|Check-Out Conference Guests</p>
    <p>&emsp;Issue Tracking</p>
    <hr class="half-rule">

    <p class="lead">Event Staff: Find Conference Guest and Report Issues</p>
    <hr class="half-rule">

    <p class="lead">Conference Guest: Report Maintenance Issue</p>
    <hr class="half-rule">
  </div>
</div>
EOT;
echo $template;
printFooterHome();

?>