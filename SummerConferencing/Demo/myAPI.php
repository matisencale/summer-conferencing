<?php
include("./adodb5/adodb.inc.php");

//Display Residence Halls Stored in the System as a Table
function printResidenceHallTableHeader() {

$tableHeader=<<<EOTH

<div>
   <table id='residencehalltable' class='table table-striped table-bordered' cellspacing='0' width='100%'>
      <thead>
         <tr>
            <th>Residence Hall ID</th>
            <th>Status ID</th>
            <th>Single Rate</th>
            <th>Double Rate</th>
         </tr>
      </thead>
      <tfoot>
         <tr>
            <th>Residence Hall ID</th>
            <th>Status ID</th>
            <th>Single Rate</th>
            <th>Double Rate</th>
         </tr>
      </tfoot>
   <tbody>
EOTH;
echo $tableHeader;
}

function printResidenceHallTableContents() {
    
    while (!$residence_hall_result->EOF) {
        for ($i=0, $max=$residence_hall_result->FieldCount(); $i < $max; $i+=4)
            $fieldResultResidenceHall.=  "<tr>\n".
                                     "<td>".$residence_hall_result->fields[$i+0]."</td>\n".
                                     "<td>".$residence_hall_result->fields[$i+1]."</td>\n".
                                     "<td>".$residence_hall_result->fields[$i+2]."</td>\n".
                                     "<td>".$residence_hall_result->fields[$i+3]."</td>\n".
                                  "</tr>\n";
            $residence_hall_result->MoveNext();
        }
    $residenceHallTableString.= $fieldResultResidenceHall;
}

function printResidenceHallTableFooter() {

$tableFooter=<<<EOTF

      </tbody>
   </table>
</div>
EOTF;
 echo $tableFooter;
}

//Php function to print javascript for DataTables Formatting
function getResidenceHallDataTable() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#residencehalltable').dataTable();";
print"       })";
print"     </script>";

}

?>