<?php

require('../myUtils.php');

printDocHeadingEvent_Staff();

$template=<<<EOT
<div class="container">
    <p class="lead">Event Staff Home</p>
    <p>&emsp;Find Guest tab used to view and search all conference guests</p>
    <p>&emsp;Conference Notes used to add notes pertaining to a particular conference</p>
    <hr class="half-rule">
</div>
EOT;
echo $template;


printFooter();

?>