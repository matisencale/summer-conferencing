<?php

require('../../myUtils.php');
require('../../../../adodb5/adodb.inc.php');

$header = <<<EOH
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

 <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js'></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.10.5/jquery.dataTables.min.js'></script>
  <script type='text/javascript' charset='utf8' src='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>
  <script type='text/javascript' charset='utf8' src='../../../TableTools-2.2.3/js/dataTables.tableTools.js'></script>

<title>

  Summer Conference Demo

</title>

<!-- Bootstrap core CSS -->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<link rel='stylesheet' type='text/css' href='../TableTools-2.2.3/css/dataTables.tableTools.css'>
<link rel='stylesheet' href='"//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap-theme.css'>
<!-- Favicons -->
<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<link rel="icon" href="/favicon.ico">

  </head>
  <body class="bs-docs-home">
    <a id="skippy" class="sr-only sr-only-focusable" href="#content"><div class="container"><span class="skiplink-text">Skip to main content</span></div></a>

    <!-- Docs master nav -->
    <header class="navbar navbar-static-top bs-docs-nav" id="top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <a href="../" class="navbar-brand">Home</a>
    </div>
  </div>
</header>
EOH;
echo $header;

print"<div class='container'>";

viewGuestTable();
getGuestDataTable();
print"</div>";
printFooter();

function viewGuestTable() {
   
   //internal testing print statement   
   //print "viewConferenceGroupTable()";

   //create db object
    $db = NewADOConnection('mysql');

    //query string
    $conference_group_query = "SELECT DISTINCT fname, lname, conference.name FROM cosc4359_guest as guest, cosc4359_conference as conference WHERE guest.conference_id = conference.conference_id";   


    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $conference_group_result = $db->Execute($conference_group_query);

    //db disconnect
    $db->Close();

    if ($conference_group_result === false) die("failed");

    

    $conferenceGroupTableString = "<div class='table-window-fit'>\n".
							  "<br></br>".
													  "<h2>Conference Group List</h2>".
                          "<table id='conferencegrouptable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>First Name</th>\n".
                          "         <th>Last Name</th>\n".
                          "         <th>Conference Name</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                          "         <th>First Name</th>\n".
                          "         <th>Last Name</th>\n".
                          "         <th>Conference Name</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with information from database-->\n";

    while (!$conference_group_result->EOF) {
        for ($i=0, $max=$conference_group_result->FieldCount(); $i < $max; $i+=3)
            $fieldResultConferenceGroup.=  "<tr>\n".
                                     "<td>".$conference_group_result->fields[$i+0]."</td>\n".
                                    "<td>".$conference_group_result->fields[$i+1]."</td>\n".
                                     "<td>".$conference_group_result->fields[$i+2]."</td>\n".

                                  "</tr>\n";
            $conference_group_result->MoveNext();
        }
    $conferenceGroupTableString.= $fieldResultConferenceGroup;
    $conferenceGroupTableString.="       </tbody>\n".
                        "   </table>\n".
					"</div>";
    echo $conferenceGroupTableString;

    print"<div class='table-window-fit'>";
     showConferenceGroupAddForm();
    print"</div>";

    print" <style type='text/css'>";
    print"    .table-window-fit{";
    print"    margin: 20px;";
    print"    }";
    print" </style>";
}



//Prints javascript for DataTable Object Initialization
function getGuestDataTable() {
  
  //internal testing print statement
  //print "getConferenceGroupDataTable()";

print "<script>\n";
print "$(document).ready(function() {\n";
print "    var table1 = $('#conferencegrouptable').DataTable();\n";
print "    var tt = new $.fn.dataTable.TableTools( table1 );\n";
print "    $( tt.fnContainer() ).insertBefore('div.dataTables_wrapper');\n";

print "    $('#conferencegrouptable tbody').on( 'mouseenter', 'tr', function () {\n";
print "        $(this).toggleClass('active');\n";
print "    } );\n";
print "    $('#conferencegrouptable tbody').on( 'mouseleave', 'tr', function () {\n";
print "        $(this).toggleClass('active');\n";
print "    } );\n";

print "    $('#conferencegrouptable tr').on( 'click','td', function () {";
print "    ar2 = $(this).attr('id');";
print "        console.log(ar2);";
print "    tablestring1 = '#conference-table-';";
print "    tablestring2 = ar2;";
print "	         fulltablestring = tablestring1.concat(tablestring2);";
print "		          console.log(fulltablestring);";
//print "    var table-heading-class=";
print "    document.location.href=fulltablestring;";

//print "        console.log(ar2);";

print "    } );";

print "} );\n";
print "</script>\n";
}

?>