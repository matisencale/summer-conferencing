<?php

include('../../myAPI.php');

printDocHeading();

$template=<<<EOT
<div class="container">
    <p class="lead">Future work includes:</p>
    <p>&emsp;View Conference Notes</p>
    <p>&emsp;Remove Pending Maintenance Requests</p>
    <p>&emsp;Add conference note form</p>
    <p>&emsp;Add maintenance request form</p>
    <hr class="half-rule">
</div>
EOT;
echo $template;


printFooter();

function printFooter() {

$footer = <<<EOF

<footer class="bs-docs-footer" role="contentinfo">
  <div class="container">
    <div class="bs-docs-social">
    </div>
  </div>
</footer>

  <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

  </body>
</html>

EOF;
echo $footer;

}

function printDocHeading() {

$header = <<<EOH
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>

  Summer Conference Demo

</title>

<!-- Bootstrap core CSS -->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">

<!-- Favicons -->
<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<link rel="icon" href="/favicon.ico">

  </head>
  <body class="bs-docs-home">
    <a id="skippy" class="sr-only sr-only-focusable" href="#content"><div class="container"><span class="skiplink-text">Skip to main content</span></div></a>

    <!-- Docs master nav -->
    <header class="navbar navbar-static-top bs-docs-nav" id="top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <a href="../" class="navbar-brand">Home</a>
    </div>
  </div>
</header>
EOH;
echo $header;

}

?>
