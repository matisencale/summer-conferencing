<?php

require('../myUtils.php');

printDocHeadingGRA();
$template=<<<EOT
<div class="container">
    <p class="lead">GRA Home</p>
    <p>&emsp;Check-In tab used to select a conference and check-in guests from a roster</p>
    <p>&emsp;Check-Out tab used to select a conference and check-ou guests from a roster</p>
    <p>&emsp;Conference Notes used to add and view notes pertaining to a particular conference</p>
    <hr class="half-rule">

    <p class="lead">Unresolved Maintenance Requests</p>
    <hr class="half-rule">
</div>
EOT;
echo $template;
printFooter();

?>