<?php

include('../../myUtils.php');
include('../../../../adodb5/adodb.inc.php');
//require('../../../Admin/admin_api3.php');

printDocHeading();
print"<div class='container'>\n";
if(empty ($_POST))
{
showSelectConferenceForm();
}
else if($_POST['selectConferenceGroupSubmit'])
{
showRosterTable($_POST['conferenceoption']);
getDataTableRoster1();
showCheckOutForm();

}
print"</div>\n";
printFooter();

function showSelectConferenceForm() {

 $self = $_SERVER['PHP_SELF'];
  $db = NewADOConnection('mysql');
  $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
  $result = $db->Execute("SELECT * FROM cosc4359_conference");

 if ($result === false) die("failed");

  print "<div> <form method='post' action='$self' class='form-horizontal>\n";
  print "<label class='col-sm-2' for='conference-option'>Select Conference (select one):</label>\n";
  print "      <select class='form-control' id='conference-option' name='conferenceoption'>\n";
  foreach($result as $k => $row) {

           echo "<option value='".$row[0]."'>".$row[1]."</option>";
  }
  print"    </select>\n";
  print "<input type='submit' name='selectConferenceGroupSubmit' ".
        " value='Select Conference to Check-In' />\n";
 print "</form>\n</div>\n";

}

function getDataTableRoster1() {

print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#rostertable').dataTable({";
print"                 'fnRowCallback': function( nRow, aData, iDisplayIndex ) {";
print"    if ( aData[6].valueOf() != '0000-00-00 00:00:00'.valueOf() && aData[7].valueOf() === '0000-00-00 00:00:0\
0'.valueOf()  )  {";
print"             console.log(typeof(aData[6]));";
print"                          console.log(nRow);";
print"         if(nRow.className == 'odd'){";
print"                                         $(nRow).addClass( 'success' ).removeClass('odd success').addClass( \
'success' )";
print"                                                                                                            \
             }";
print"         if(nRow.className == 'even'){";
print"          $(nRow).addClass( 'success' ).removeClass('even success').addClass( 'success' )";
print"          }";
print"          }";

print"    if ( aData[7].valueOf() != '0000-00-00 00:00:00'.valueOf() )  {";
print"         console.log(typeof(aData[6]));";
print"         console.log(nRow);";
print"         if(nRow.className == 'odd'){";
print"          $(nRow).addClass( 'danger' ).removeClass('odd danger').addClass( 'danger' )";
print"          }";
print"         if(nRow.className == 'even'){";
print"          $(nRow).addClass( 'danger' ).removeClass('even warning').addClass( 'danger' )";
print"          }";
print"                              }";

print"    if ( aData[7].valueOf() != '0000-00-00 00:00:00'.valueOf() )  {";
print"         console.log(typeof(aData[6]));";
print"         console.log(nRow);";
print"          $(nRow).addClass( 'warning' ).removeClass('odd warning').addClass( 'warning' )";
print"              }";

print"                          }";
print"                                               });";

print "    $('#rostertable tbody').on( 'click', 'tr', function () {\n";
print "        $(this).toggleClass('active');\n";
print "    } );\n";


print "    $('#rostertable tr').on('click', 'td', function () {";
print "        $(this).toggleClass('active');\n";


print "    ar5 =$(this).attr('id');";
print "            document.getElementById('roster-guest-selection-input2').value = ar5;";

print "        console.log(ar5);";

print "    } );";

print"       })";
print"     </script>";
}

function showCheckInForm() {

  print "<div> <form method='post' action='$self' >\n";
  print "<input type='hidden' name='conferenceoption' value='".$_POST['conferenceoption']."' />\n";
  print "<input type='hidden' id='roster-guest-selection-input1' name='rosterguestid' value='' />\n";
  print "<input type='submit' name='selectRosterGuestCheckInSubmit' value='Check-In ' />\n";
  print "</form>\n</div>\n";
  print "<br></br>";
}

function showCheckOutForm() {

  print "<div> <form method='post' action='$self' >\n";
  print "<input type='hidden' name='conferenceoption' value='".$_POST['conferenceoption']."' />\n";
  print "<input type='hidden' id='roster-guest-selection-input2' name='rosterguestid' value='' />\n";
  print "<input type='submit' name='selectRosterGuestCheckOutSubmit' value='Check-Out ' />\n";
  print "</form>\n</div>\n";
  print "<br></br>";

}

function showRosterTable($conferenceID) {
 //create db object
    $db = NewADOConnection('mysql');

    //query string

    $conference_name_query = "SELECT name FROM `cosc4359_conference` WHERE conference_id=".$conferenceID."";

    $roster_query = "SELECT DISTINCT guest.guest_id, fname, lname, gender.gender, conference.name, residence_hall_name, room_name, check_in, check_out FROM \
cosc4359_guest as guest, cosc4359_gender as gender, cosc4359_conference as conference, cosc4359_residence_hall as residence_hall, cosc4359_room as room, cos\
c4359_roster as roster, cosc4359_key as k WHERE guest.conference_id = conference.conference_id AND guest.room_id=room.room_id AND room.residence_hall_id=res\
idence_hall.residence_hall_id AND guest.guest_id=roster.guest_id AND guest.gender_id=gender.gender_id AND conference.conference_id=".$conferenceID."";



//db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $roster_result = $db->Execute($roster_query);
    $conference_name_result = $db->Execute($conference_name_query);

    //db disconnect
    $db->Close();

    if ($roster_result === false) die("failed");

//    print $conference_name;
    print "<div class='table-window-fit'>";
    print "<h2><a id='roster-table'></a>".$conference_name_result->fields[0]." Roster</h2>";
//    print $conference_name->fields[0];
    $table_id.="rostertable";
    $rosterTableString = "<div>\n".
                          "<table id='".$table_id."' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
                          "         <th>First Name</th>\n".
                          "         <th>Last Name</th>\n".
                          "         <th>Gender</th>\n".
                          "         <th>Conference Name</th>\n".
                         "         <th>Residence Hall</th>\n".
                          "         <th>Room</th>\n".
                          "         <th>Check-In</th>\n".
                          "         <th>Check-Out</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
                        "         <th>First Name</th>\n".
                          "         <th>Last Name</th>\n".
                          "         <th>Gender</th>\n".
                          "         <th>Conference Name</th>\n".
                         "         <th>Residence Hall</th>\n".
                          "         <th>Room</th>\n".
                          "         <th>Check-In</th>\n".
                          "         <th>Check-Out</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with information from database-->\n";


    while (!$roster_result->EOF) {
        for ($i=0, $max=$roster_result->FieldCount(); $i < $max; $i+=9)
            $fieldResultRoster.=  "<tr>\n".
                                     "<td id=".$roster_result->fields[$i+0].">".$roster_result->fields[$i+1]."</td>\n".
                                     "<td>".$roster_result->fields[$i+2]."</td>\n".
                                     "<td>".$roster_result->fields[$i+3]."</td>\n".
                                     "<td>".$roster_result->fields[$i+4]."</td>\n".
                                     "<td>".$roster_result->fields[$i+5]."</td>\n".
                                     "<td>".$roster_result->fields[$i+6]."</td>\n".
                                     "<td>".$roster_result->fields[$i+7]."</td>\n".
                                     "<td>".$roster_result->fields[$i+8]."</td>\n".
                                  "</tr>\n";
            $roster_result->MoveNext();
        }
    $rosterTableString.= $fieldResultRoster;
    $rosterTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $rosterTableString;
   // getRosterDataTable();

       print" <style type='text/css'>";
    print"    .table-window-fit{";
    print"    margin: 20px;";
    print"    }";
    print" </style>";

}



?>
