<?php

printDocHeading();
print"<body>\n";
if(empty ($_POST))
{
 showLoginForm();
}
else if ($_POST['submitLogin'])
{
  checkLoginFormData();
}

?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="./bootstrap-3.3.2/dist/js/bootstrap.min.js"></script>
    <script src="./bootstrap-3.3.2/docs/assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./bootstrap-3.3.2/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>

<?php
function printDocHeading() {

print"<!DOCTYPE html>\n";
print"<html lang='en'>\n";
print"  <head>\n";
print"    <meta charset='utf-8'>\n";
print"    <meta http-equiv='X-UA-Compatible' content='IE=edge'>\n";
print"    <meta name='viewport' content='width=device-width, initial-scale=1'>\n";
print"    <meta name='description' content=''>\n";
print"    <meta name='author' content=''>\n";
print"    <link rel='icon' href='./CSS/tower_favicon_32x32.png'>\n";

print"    <title>SEU Summer Conferencing</title>\n";

print"    <!-- Bootstrap core CSS -->\n";
print"    <link href='./bootstrap-3.3.2/dist/css/bootstrap.min.css' rel='stylesheet'>\n";

print"    <!-- Custom styles for this template -->\n";
print"    <link href='./index.css' rel='stylesheet'>\n";

print"	    </head>\n";

}

function showLoginForm($aUserName="", $aPassword="") {

$self = $_SERVER['PHP_SELF'];

print"    <div class='site-wrapper'>\n";
print"      <div class='site-wrapper-inner'>\n";
print"        <div class='cover-container'>\n";
print"          <h1 class='cover-heading'>St Edward's Summer Conferencing</h1>\n";
print"          </br>\n";
print"          <div class='inner cover'>\n";
print"            <h1 class='cover-heading'><img src='./CSS/hilltopper.png'</h1>\n";

print"            <form class='form-signin' method='post' action='$self'>\n";
//print"              <h4 class='form-signin-heading'>Please sign in</h4>\n";
//print"              <input type='text' id='inputUsername' name='theUserName' class='form-control' placeholder='Username' value='$aUserName' required autofocus>\n";
//print"              <input type='password' name='thePassword' value='$aPassword' id='inputPassword' class='form-control' placeholder='Password' required>\n";
print"              <div class='checkbox'>\n";
print"              </div>\n";
//print"              <button class='btn btn-lg btn-default btn-block' name='submitLogin' type='submit'>Sign in</button>\n";

print"              <a href='http://www.cs.stedwards.edu/~mharper5/SummerConferencing/Admin/index2.php' class='btn btn-lg btn-default btn-block' role='button'>Admin</a>\n";
print"              <a href='http://www.cs.stedwards.edu/~mharper5/SummerConferencing/User/index2.php' class='btn btn-lg btn-default btn-block' role='button'>User</a>\n";

print"            </form>\n";

print"          </div>\n";
print"          <div class='mastfoot'>\n";
print"          </div>\n";
print"        </div>\n";
print"      </div>\n";
print"    </div>\n";
}

function checkLoginFormData() {

  $username = htmlentities($_POST['aUsername'], ENT_QUOTES);
  $password = htmlentities($_POST['aPassword'], ENT_QUOTES);
  $errorMessage = "";
  if($username == "")
  {
    $errorMessage .= "<h3> You must enter your Username: </h3>\n";
  }
  if($password == "" )
  {
    $errorMessage .= "<h3> You must enter a password </h3>\n";
  }
  if($errorMessage)
  {
    print $errorMessage;
    showLoginForm($username, $password);
  }
print "POST array: \n";
  print_r($_POST);

}
?>
