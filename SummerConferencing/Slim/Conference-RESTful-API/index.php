<?php

require 'vendor/autoload.php';

$app = new \Slim\Slim();

// GET route

//conference groups
$app->get('/conferencegroups', 'getConferencegroups'); // Using Get HTTP Method and process getConferencegroups function

/*
$app->get('/conferencegroups/:conferencegroupid', 'getConferencegroup'); // Using Get HTTP Method and process getConferencegroup function
$app->post('/conferencegroups', 'addConferencegroup'); // Using Post HTTP Method and process addConferencegroup function
$app->put('/conferencegroups/:conferencegroupid', 'updateConferencegroup'); // Using Put HTTP Method and process updateConferencegroup function
$app->delete('/conferencegroups/:conferencegroupid', 'deleteConferencegroup'); // Using Delete HTTP Method and process deleteConferencegroup function
*/

/*
//conferences
$app->get('/allconferences', 'getAllconferences'); // Using Get HTTP Method and process getAllconferences function
$app->get('/conferences/:conferenceid', 'getConference'); // Using Get HTTP Method and process getConference function
$app->post('/conferences', 'addConference'); // Using Post HTTP Method and process addConference function
$app->put('/conferences/:conferenceid', 'updateConference'); // Using Put HTTP Method and process updateConference function
$app->delete('/conferences/:conferenceid', 'deleteConference'); // Using Delete HTTP Method and process deleteConference function

//residencehalls
$app->get('/residencehalls', 'getResidencehalls'); // Using Get HTTP Method and process getResidencehalls function
$app->get('/residencehalls/:residencehallid', 'getResidencehall'); // Using Get HTTP Method and process getResidencehall function
$app->post('/residencehalls/:', 'addResidencehall'); // Using Post HTTP Method and process addResidencehall function
$app->put('/residencehalls/:residencehallid', 'updateResidencehall'); // Using Put HTTP Method and process updateResidencehall function
$app->delete('/residencehalls/:residencehallid', 'deleteResidencehall'); // Using Delete HTTP Method and process deleteResidencehall function


$app->get('/residencehallrooms', 'getResidencehallrooms'); // Using Put HTTP Method and process updateResidencehall function


$app->get('/guests/:conferenceid', 'getGuests'); // Using Get HTTP Method and process getConferencegroups function
$app->get('/conferencegroups', 'getConferencegroups'); // Using Get HTTP Method and process getConferencegroups function
*/


function getConferencegroups() {

    $sql_query = "select * FROM cosc4359_conference_group";
    try {
        $dbCon = getConnection();
        $stmt   = $dbCon->query($sql_query);
        $conferencegroups  = $stmt->fetchAll(PDO::FETCH_OBJ);
        $dbCon = null;
	echo json_encode($conferencegroups);
    }
    catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }    
}


$app->run();

function getConnection() {
    try {
        $db_username = "mharper5";
        $db_password = "madisen";
        $conn = new PDO('mysql:host=db01.cs.stedwards.edu;dbname=mharper5', $db_username, $db_password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch(PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
    return $conn;
}
