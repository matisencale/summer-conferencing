<?php
function printAdminNavBar() {

print" <div class='bs-example'>\n";
print"    <nav role='navigation' class='navbar navbar-custom navbar-fixed-top'>\n";
print"        <!-- Brand and toggle get grouped for better mobile display -->\n";
print"        <div class='navbar-header'>\n";
print"            <button type='button' data-target='#navbarCollapse' data-toggle='collapse' class='navbar-toggle'>\n";
print"                <span class='sr-only'>Toggle navigation</span>\n";
print"                <span class='icon-bar'></span>\n";
print"                <span class='icon-bar'></span>\n";
print"                <span class='icon-bar'></span>\n";
print"            </button>\n";
print"            <a href='http://www.cs.stedwards.edu/~mharper5/SummerConferencing/index.php' class='navbar-brand'>Home</a>\n";
print"        </div>\n";
print"        <!-- Collection of nav links, forms, and other content for toggling -->\n";
print"        <div id='navbarCollapse' class='collapse navbar-collapse'>\n";
print"            <ul class='nav navbar-nav'>\n";
print"                <li class='active'><a href='#'>Conferences</a></li>\n";
print"                <li><a href='#'>Residence Halls</a></li>\n";
print"            </ul>\n";
print"            <ul class='nav navbar-nav navbar-right'>\n";
print"            </ul>\n";
print"            <ul class='nav navbar-nav navbar-right'>\n";
print"                <li><a href='#'>Login</a></li>\n";
print"            </ul>\n";
print"            <form role='search' class='navbar-form navbar-right'>\n";
print"                <div class='form-group'>\n";
print"                    <input type='text' placeholder='Find Guest' class='form-control'>\n";
print"                </div>\n";
print"            </form>\n";
print"        </div>\n";
print"        </div>\n";
print"    </nav>\n";
print" </div>\n";
}

function printFooter() {
print" <footer class='footer'>";
print" <div class=' navbar-fixed-bottom'>";
print" <div class='container'>";
print" <div class='col-4 col-offset-4' style='text-align:center;'>";
print"   <p style='vertical-align:middle;'><a href='#'class='backToTop'>Back To Top</a></p>";
print" </div>";
print" </div>";
print"    </footer>";

print"<script type='text/javascript'>";
print"jQuery('.backToTop').click(function(){";
print"  jQuery('html, body').animate({scrollTop:0}, 'slow');";
print"});";
print"</script>";
//print"<script>";
//print"$('body').css('overflow','hidden');";
//print"$('body').css('overflow','auto');";
//print</script>":
}

function printDocHeading() {
print" <!DOCTYPE html>\n";
print" <html lang='en'\n>";
print" <head>\n";
print" <meta charset='UTF-8'>\n";

print" <title>SEU Summer Conferencing</title>\n";
print" <link rel='stylesheet' href='./bootstrap-3.3.2/dist/css/bootstrap.min.css'>\n";

print" <link rel='stylesheet' type='text/css' href='./TableTools-2.2.3/css/dataTables.tableTools.css'>";

print" <link rel='stylesheet' href='./bootstrap-3.3.2/dist/css/bootstrap-theme.min.css'>\n";
print" <link rel='stylesheet' href='./bootstrap-3.3.2/dist/css/bootstrap.custom.css'>\n";
print" <link rel='stylesheet' type='text/css' href='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css'>";

print"    <link rel='icon' href='./CSS/tower_favicon_32x32.png'>\n";
print"    <!-- Custom styles for this template -->\n";
print"    <link href='./index.css' rel='stylesheet'>\n";

print" <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>\n";
print" <script src='./bootstrap-3.3.2/dist/js/bootstrap.min.js'></script>\n";

print" </style>\n";
print" </head>\n";
}

function showLoginForm($aUserName="", $aPassword="") {

$self = $_SERVER['PHP_SELF'];

print"    <div class='site-wrapper'>\n";
print"      <div class='site-wrapper-inner'>\n";
print"        <div class='cover-container'>\n";
print"          <h1 class='cover-heading'>St Edward's Summer Conferencing</h1>\n";
print"          </br>\n";
print"          <div class='inner cover'>\n";
print"            <h1 class='cover-heading'><img src='./CSS/hilltopper.png'</h1>\n";
print" 		  <br></br>";
print"            <form class='form-signin' method='post' action='$self'>\n";

print"              <input type='submit' class='btn btn-lg btn-default btn-block' id='submitAdminLogin' name='adminLogin' value='Admin Login'></input>\n";
print"              <input type='submit' class='btn btn-lg btn-default btn-block' id='submitUserLogin' name='userLogin' value='User Login'></input>\n";

print"            </form>\n";

print"          </div>\n";
print"          <div class='mastfoot'>\n";
print"          </div>\n";
print"        </div>\n";
print"      </div>\n";
print"    </div>\n";
}

?>
