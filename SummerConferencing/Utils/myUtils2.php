<?php

function printDocHeading()
{ 
  print
    '<!DOCTYPE html>'. "\n" .
    '<html lang="en">' . "\n" .
    '<head>' . "\n" .
    'link rel="stylesheet" href="../bootstrap-3.3.2/dist/css/bootstrap.min.css">'. "\n".
    'link rel="stylesheet" href="../bootstrap-3.3.2/dist/css/bootstrap-theme.min.css">'. "\n".
    'link rel="stylesheet" href="../bootstrap-3.3.2/dist/css/bootstrap.custom.css">'. "\n".
    'script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>'. "\n".
    'script src="../bootstrap-3.3.2/dist/js/bootstrap.min.js"></script>'. "\n".
    '<title>SEU Summer Conferencing</title>'. "\n" .
    '</head>'. "\n";
}
<?php
