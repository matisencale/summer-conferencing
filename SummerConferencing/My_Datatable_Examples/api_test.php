<?php

$template = <<<EOT
<!DOCTYPE html PUBLIC "-//IETF//DTD HTML 2.0//EN">
<HTML>
   <HEAD>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css">
      <TITLE>
         Datatables testing for json object 
      </TITLE>
   </HEAD>
<BODY>
   <div>
	<table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Conference Group ID</th>
                    <th>Conference Group Name</th>
                </tr>
            </thead>
 
            <tfoot>
                <tr>
                    <th>Conference Group ID</th>
                    <th>Conference Group Name</th>
                </tr>
            </tfoot>
        </table>
   </div>

</BODY>

<script>
$(document).ready(function() {

   var request = $.ajax({
       url: "http://www.cs.stedwards.edu/~mharper5/SummerConferencing/Slim/Conference-RESTful-API/index.php/conferencegroups",
       type: "GET",
       dataType: "html"
   });
    $('#example').dataTable( {
        'ajax': JSON.parse(request);
    } );
} );
</script>

</HTML>

EOT;

echo $template
?>