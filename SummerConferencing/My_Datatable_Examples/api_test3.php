<?php

$template = <<<EOT
<!DOCTYPE html PUBLIC "-//IETF//DTD HTML 2.0//EN">
<HTML>
   <HEAD>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css">
      <TITLE>
         Datatables testing for json object 
      </TITLE>
   </HEAD>
<BODY>
   <div>
	<table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Conference Group ID</th>
                    <th>Conference Group Name</th>
                </tr>
            </thead>
 
            <tfoot>
                <tr>
                    <th>Conference Group ID</th>
                    <th>Conference Group Name</th>
                </tr>
            </tfoot>
        </table>
   </div>

</BODY>

<script>

function foo() {
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', "http://www.cs.stedwards.edu/~mharper5/SummerConferencing/Slim/Conference-RESTful-API/index.php/conferencegroups");
    httpRequest.send();
    return httpRequest.responseText;
}

var result = foo();

console.log(result);

$(document).ready(function() {
    $('#demo').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>' );
 
    $('#example').dataTable( {
        "data": [{"conference_group_id":"1","conference_group_name":"Conference Group Testing"},{"conference_group_id":"2","conference_group_name":"Summer 2015"},{"conference_group_id":"9","conference_group_name":"Summer 2014"},{"conference_group_id":"10","conference_group_name":"Summer 2013"},{"conference_group_id":"11","conference_group_name":"Senior Symposium"},{"conference_group_id":"12","conference_group_name":"Test Group 1"},{"conference_group_id":"13","conference_group_name":"Test Group 2"}],
        "columns": [
            { "conference_group_id": "Conference Group ID" },
            { "conference_group_name": "Conference Group Name" }
        ]
    } );   
} );

</script>

</HTML>

EOT;

echo $template
?>