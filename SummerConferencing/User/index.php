<?php
include("../../adodb5/adodb.inc.php");

printDocHeading();
print" <body>\n";
printNavBar();
printConferenceGroupTable();
//getDataTableConferenceGroup();
?>
<script>
$(document).ready(function() {
    var table = $('#conferencegrouptable').DataTable();
 
    $('#conferencegrouptable tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('active');
    } );
 
    $('td').on('click', function () {
    ar =$(this).attr("id");
        console.log(ar);

    } );
} );
</script>


<button type="button" id='button' class="btn btn-active">Action</button>
<?php

function printConferenceGroupTable() {
    //create db object
    $db = NewADOConnection('mysql');

    //query string
    $conference_group_query = "SELECT * FROM cosc4359_conference_group";

    //db connect
    $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");

    //query results
    $conference_group_result = $db->Execute($conference_group_query);

    //db disconnect
    $db->Close();

    if ($conference_group_result === false) die("failed");

    $conferenceGroupTableString = "<div class='table-window-fit'>\n".
                          "<table id='conferencegrouptable' class='table table-striped table-bordered' cellspacing='0' width='100%'>\n".
                          "   <thead>\n".
                          "      <tr>\n".
//                          "         <th>Conference GroupID</th>\n".
                          "         <th>Conference Group</th>\n".
                          "      </tr>\n".
                          "   </thead>".

                          "   <tfoot>\n".
                          "      <tr>\n".
//                          "         <th>Conference Group ID</th>\n".
                          "         <th>Conference Group</th>\n".
                          "      </tr>\n".
                          "   </tfoot>\n".

                          "   <tbody>\n".
                          "   <!-- populating body with information from database-->\n";

    while (!$conference_group_result->EOF) {
        for ($i=0, $max=$conference_group_result->FieldCount(); $i < $max; $i+=4)
            $fieldResultConferenceGroup.=  "<tr>\n".
//                                     "<td>".$conference_group_result->fields[$i+0]."</td>\n".
                                     "<td class='conferencegroupname' id=".$conference_group_result->fields[$i+0].">".$conference_group_result->fields[$i+1]."</td>\n".
                                  "</tr>\n";
            $conference_group_result->MoveNext();
        }
    $conferenceGroupTableString.= $fieldResultConferenceGroup;
    $conferenceGroupTableString.="       </tbody>\n".
                        "   </table>\n".
                        "</div>";
    echo $conferenceGroupTableString;
    print" <style type='text/css'>";
    print"    .table-window-fit{";
    print"    margin: 20px;";
    print"    }";
    print" </style>";


}

//prints javascript for building DataTables object
function getDataTableConferenceGroup() {
/*
print"     <!-- Status Table -->";
print"     <script>";
print"       $(document).ready(function(){";
print"       $('#conferencegrouptable').dataTable();";
print"       })";
print"     </script>";
*/

}

function printDocHeading() {
print" <!DOCTYPE html>\n";
print" <html lang='en'\n>";
print" <head>\n";
print" <meta charset='UTF-8'>\n";
print" <title>SEU Summer Conferencing User</title>\n";
print" <link rel='stylesheet' href='../bootstrap-3.3.2/dist/css/bootstrap.min.css'>\n";
print" <link rel='stylesheet' href='../bootstrap-3.3.2/dist/css/bootstrap-theme.min.css'>\n";
print" <link rel='stylesheet' href='../bootstrap-3.3.2/dist/css/bootstrap.custom.css'>\n";
print" <link rel='stylesheet' type='text/css' href='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css'>";

print" <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>\n";
print" <script src='../bootstrap-3.3.2/dist/js/bootstrap.min.js'></script>\n";

print" </style>\n";
print" </head>\n";
}

function printNavBar() {
print" <div class='bs-example'>\n";
print"    <nav role='navigation' class='navbar navbar-custom'>\n";
print"        <!-- Brand and toggle get grouped for better mobile display -->\n";
print"        <div class='navbar-header'>\n";
print"            <button type='button' data-target='#navbarCollapse' data-toggle='collapse' class='navbar-toggle'>\n";
print"                <span class='sr-only'>Toggle navigation</span>\n";
print"                <span class='icon-bar'></span>\n";
print"                <span class='icon-bar'></span>\n";
print"                <span class='icon-bar'></span>\n";
print"            </button>\n";
print"            <a href='http://www.cs.stedwards.edu/~mharper5/SummerConferencing' class='navbar-brand'>Home</a>\n";
print"        </div>\n";
print"        <!-- Collection of nav links, forms, and other content for toggling -->\n";
print"        <div id='navbarCollapse' class='collapse navbar-collapse'>\n";
print"            <ul class='nav navbar-nav'>\n";
/*
print"                <li class='active'><a href='#'>Conferences</a></li>\n";
print"                <li><a href='#'>Residence Halls</a></li>\n";
print"                <li class='dropdown'>\n";
print"                    <a data-toggle='dropdown' class='dropdown-toggle' href='#'>View Reports <b class='caret'></b></a>\n";
print"                    <ul role='menu' class='dropdown-menu'>\n";
print"                        <li><a href='#'>Guests On Campus</a></li>\n";
print"                        <li class='divider'></li>\n";
print"                        <li><a href='#'>Maintenance</a></li>\n";
print"                    </ul>\n";
print"                </li>\n";
print"            </ul>\n";
print"            <form role='search' class='navbar-form navbar-left'>\n";
print"                <div class='form-group'>\n";
print"                    <input type='text' placeholder='Find Guest' class='form-control'>\n";
print"                </div>\n";
print"            </form>\n";
print"            <ul class='nav navbar-nav navbar-right'>\n";
print"                <li><a href='#'>Login</a></li>\n";
*/
print"            </ul>\n";
print"        </div>\n";
print"    </nav>\n";
print" </div>\n";

}

/*
<div class="row">
    <div class="col-sm-6 col-md-2">
          <div class="thumbnail">
	          <img src="..." alt="...">
		        <div class="caption">
			          <h3>Conference_1</h3>
				          <p>Conference Description...</p>
					          <p><a href="#" class="btn btn-primary" role="button">Check-In</a> <a href="#" class="btn btn-default" role="button">Check-Out</a></p>
			</div>
	  </div>
    </div>
    <div class="col-sm-6 col-md-2">
          <div class="thumbnail">
	          <img src="..." alt="...">
		        <div class="caption">
			          <h3>Conference_2</h3>
				          <p>Description</p>
					          <p><a href="#" class="btn btn-primary" role="button">Check-In</a> <a href="#" class="btn btn-default" role="button">Check-Out</a></p>
						        </div>
			    </div>
	    </div>
</div>
*/

print"     <!-- JQuery -->";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.10.5/jquery.dataTables.min.js'></script>";
print"     <script type='text/javascript' charset='utf8' src='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>";



print" </body>\n";
print" </html>\n";						
?>