<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Example of Bootstrap 3 Static Navbar Extended</title>
<link rel="stylesheet" href="../bootstrap-3.3.2/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="../bootstrap-3.3.2/dist/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="../bootstrap-3.3.2/dist/css/bootstrap.custom.css">
 <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.10.5/css/jquery.dataTables.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../bootstrap-3.3.2/dist/js/bootstrap.min.js"></script>

<style type="text/css">
    .bs-example{
    margin: 20px;
    }
</style>
</head> 
<body>
<div class="bs-example">
    <nav role="navigation" class="navbar navbar-custom">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Brand</a>
        </div>
        <!-- Collection of nav links, forms, and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Conferences</a></li>
                <li><a href="#">Residence Halls</a></li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">View Reports <b class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="#">Guests On Campus</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Maintenance</a></li>
                    </ul>
                </li>
            </ul>
            <form role="search" class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" placeholder="Find Guest" class="form-control">
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Login</a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="container">
<?php
include("../../adodb5/adodb.inc.php");

 $db = NewADOConnection('mysql');
 $db->Connect("db01.cs.stedwards.edu", "mharper5", "madisen", "mharper5");
 $result = $db->Execute("SELECT guest.guest_id, fname, lname, gender, conference_name, residence_hall_name, room_number, check_in, check_out FROM cosc4157_guest as guest, cosc4157_conference as conference, cosc4157_residence_hall as residence_hall, cosc4157_room as room, cosc4157_roster as roster WHERE guest.conference_id = conference.conference_id AND guest.room_id=room.room_id AND room.residence_hall_id=residence_hall.residence_hall_id AND guest.guest_id=roster.roster_id");

 if ($result === false) die("failed");
?>
     <table id="datatables" class="display" cellspacing="0" width="100%">
         <thead>
	       <tr>
		 <th>Guest ID</th>
		 <th>First Name</th>
                <th>Last Name</th>
                <th>Gender</th>
		<th>Conference</th>
		<th>Residence Hall</th>
		<th>Room Number</th>
		<th>Check-in</th>
		<th>Check-out</th>
            </tr>
         </thead>
 
	  <tfoot>
	        <tr>
		  <th>Guest ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Gender</th>
                <th>Conference</th>
                <th>Residence Hall</th>
                <th>Room Number</th>
                <th>Check-in</th>
                <th>Check-out</th>
		    </tr>
         </tfoot>
	   <tbody>
<!-- populating body with guest information from database-->
<?php
while (!$result->EOF) {
     for ($i=0, $max=$result->FieldCount(); $i < $max; $i+=9)
        $fieldResult.=  "<tr>\n".
						 "<td>".$result->fields[$i+0]."</td>\n".
                        "<td>".$result->fields[$i+1]."</td>\n".
                        "<td>".$result->fields[$i+2]."</td>\n".
"<td>".$result->fields[$i+3]."</td>\n".
                        "<td>".$result->fields[$i+4]."</td>\n".
                        "<td>".$result->fields[$i+5]."</td>\n".
"<td>".$result->fields[$i+6]."</td>\n".
                        "<td>".$result->fields[$i+7]."</td>\n".
                        "<td>".$result->fields[$i+8]."</td>\n".

                        "</tr>\n";
      $result->MoveNext();
  }
print $fieldResult;
?>
 </tbody>
     </table>
     <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
     <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
 <script>
$(document).ready(function() {
    var table = $('#datatables').DataTable();
 
    $('#datatables tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    $('#checkin').click( function () {
        console.log(table.row('.selected',0).data());
    } );
} );
     </script>  
<div class="btn-group">
       <a href="" class="btn btn-success" id="checkin"><span class="glyphicon glyphicon-ok"></span> Check-In</a>
     </div>
     <div class="btn-group">
       <a href="#" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Guest</a>
     </div>

</div>


</body>
</html>
